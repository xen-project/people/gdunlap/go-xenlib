package xenversion

import (
	"testing"
)

func TestParseXenVersion(t *testing.T) {
	var tests = []struct {
		s             string
		shouldSucceed bool
		major, minor  int
	}{
		{"4.8", true, 4, 8},
		{"4.11", true, 4, 11},
		{"3.4", true, 3, 4},
		{"master", true, xenVersionMasterMajor, 0},
		{"xen-unstable", true, xenVersionMasterMajor, 0},
		{"4.11.x", true, 4, 11},
		{"4.15.x.", true, 4, 15},
		{"4.11-stable", true, 4, 11},
		{"4.11-rc", true, xenVersionMasterMajor, 0},
		{"4.11-rc7", true, xenVersionMasterMajor, 0},
		{"4", false, -1, -1},
		{"4a.11", false, -1, -1},
		{"4.11a", false, -1, -1},
		{"4.", false, -1, -1},
		{"4.12.", false, -1, -1},
		{"4.12.1", false, -1, -1},
		{"4.12.1.1", false, -1, -1},
	}

	for _, test := range tests {
		v, err := XenVersionFromString(test.s)
		if test.shouldSucceed && err != nil {
			t.Errorf("Unexpected failure:  %s returned error %v", test.s, err)
			return
		} else if !test.shouldSucceed && err == nil {
			t.Errorf("Unexpected success: %s", test.s)
			return
		}

		if test.shouldSucceed {

			major, minor, err := v.parse()
			if err != nil {
				t.Errorf("Error parsing supposedly-valid version: %v", err)
				return
			}
			if major != test.major {
				t.Errorf("Error parsing %s: Expected major %d, got %d",
					test.s, test.major, major)
				return
			}
			if minor != test.minor {
				t.Errorf("Error parsing %s: Expected minor %d, got %d",
					test.s, test.minor, minor)
				return
			}
		}
	}
}

func TestParseXenVersionFull(t *testing.T) {
	var tests = []struct {
		s              string
		shouldSucceed  bool
		releaseVersion XenVersion
		point          int
	}{
		{"4.8", false, XenVersionMin, 0},
		{"4.11", false, XenVersionMin, 0},
		{"3.4", false, XenVersionMin, 0},
		{"master", true, XenVersionMaster, 0},
		{"4", false, XenVersionMin, 0},
		{"4a.11", false, XenVersionMin, 0},
		{"4.11a", false, XenVersionMin, 0},
		{"4.", false, XenVersionMin, 0},
		{"4.12.", false, XenVersionMin, 0},
		{"4.12.1", true, XenVersion("4.12"), 1},
		{"4.8.3", true, XenVersion("4.8"), 3},
		{"4.12.1.1", false, XenVersionMin, 0},
	}

	for _, test := range tests {
		fv, err := XenVersionFullFromString(test.s)
		if test.shouldSucceed && err != nil {
			t.Errorf("Unexpected failure:  %s returned error %v", test.s, err)
			return
		} else if !test.shouldSucceed && err == nil {
			t.Errorf("Unexpected success: %s", test.s)
			return
		}

		if test.shouldSucceed {
			v, point, err := fv.parse()
			if err != nil {
				t.Errorf("Error parsing supposedly-valid version: %v", err)
				return
			}
			if v != test.releaseVersion {
				t.Errorf("Error parsing %s: Expected major %v, got %v",
					test.s, test.releaseVersion, v)
				return
			}
			if point != test.point {
				t.Errorf("Error parsing %s: Expected minor %d, got %d",
					test.s, test.point, point)
				return
			}
		}
	}
}
