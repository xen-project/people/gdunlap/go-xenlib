package xenversion

import "testing"

func TestFVRMerge(t *testing.T) {
	var tests = []struct {
		a, b, want    XenVersionFullRange
		shouldSucceed bool
	}{
		{XenVersionFullRange{XenVersionFull(""), XenVersionFull("")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			true},
		{XenVersionFullRange{XenVersionFullInvalid, XenVersionFullInvalid},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.3")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.3")},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFullMaster},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.2")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFullMaster},
			true},
		{XenVersionFullRange{XenVersionFull("4.11.1"), XenVersionFull("4.11.3")},
			XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{},
			false},
		{XenVersionFullRange{XenVersionFull("4.12.1"), XenVersionFull("4.11.3")},
			XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{},
			false},
		{XenVersionFullRange{XenVersionFull("4.12.1"), XenVersionFull("4.12.3")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFull("4.11.2")},
			XenVersionFullRange{},
			false},
		{XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.13.0")},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.11.0"), XenVersionFullMaster},
			true},
		{XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.13.0")},
			XenVersionFullRange{XenVersionFull("4.13.0"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFullMaster},
			true},
		{XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.13.0"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFullMaster},
			true},
		{XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.13.0")},
			XenVersionFullRange{XenVersionFull("4.14.0"), XenVersionFull("4.15.0")},
			XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.15.0")},
			true},
		{XenVersionFullRange{XenVersionFull("4.12.0"), XenVersionFull("4.12.1")},
			XenVersionFullRange{XenVersionFull("4.14.0"), XenVersionFull("4.15.0")},
			XenVersionFullRange{},
			false},
		{XenVersionFullRange{XenVersionFull("4.12.1"), XenVersionFullMaster},
			XenVersionFullRange{XenVersionFull("4.14.0"), XenVersionFull("4.15.0")},
			XenVersionFullRange{},
			false},
	}

	for _, test := range tests {
		got := test.a
		err := got.Merge(test.b)
		if test.shouldSucceed && err != nil {
			t.Errorf("Unexpected failure:  %v Merge (%v) returned error %v",
				test.a, test.b, err)
			return
		} else if !test.shouldSucceed && err == nil {
			t.Errorf("Unexpected success: %v Merge (%v)", test.a, test.b)
			return
		}

		if test.shouldSucceed {
			if got.Start != test.want.Start ||
				got.End != test.want.End {
				t.Errorf("ERROR: Mismatch: %v Merge (%v) got %v wanted %v!",
					test.a, test.b, got, test.want)
				return
			}
		}
	}
}
