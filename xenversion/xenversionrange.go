package xenversion

import (
	"encoding/json"
	"fmt"
)

type XenVersionRange struct {
	Start XenVersion
	End   XenVersion
}

// Version ranges can take one of several forms:
//
// ["a.b.x"]: The single major version, all point releases, a.b.0 through a.b.current
// [null, "a.b.x"] All Xen versions through a.b.x
// ["a.b.x", null] All xen versions starting with a.b.x until current version, including master
//
// Naturally, "current" means, "current at the time the XSA was
// issued"; so to find the exact versions you need to know when
// various releases happened.
//
// Also, some strings have "a.b.c", if a vulnerability was introduced
// while backporting something to a stable branch.  For the time being
// this is ignored.
func (vr *XenVersionRange) UnmarshalJSON(b []byte) error {
	var vListString []string
	err := json.Unmarshal(b, &vListString)
	if err != nil {
		return fmt.Errorf("VersionRange: Unmarshalling []string: %v", err)
	}

	if len(vListString) > 2 || len(vListString) == 0 {
		return fmt.Errorf("VersionRange: Invalid range: %d elements",
			len(vListString))
	}

	if vListString[0] == "" {
		// Empty start?  XenVersionMin
		vr.Start = XenVersionMin
	} else if start, err := XenVersionFromString(vListString[0]); err == nil {
		// Try parsing it as a minor release ("a.b", "a.b.x", "master", or "a.b-rc")
		vr.Start = start
	} else if startFull, err := XenVersionFullFromString(vListString[0]); err == nil {
		// Try parsing it as a point release ("a.b.c")
		vr.Start = startFull.XenVersion()
	} else {
		return fmt.Errorf("VersionRange: Cannot parse first value of %s as a XenVersion or XenVersionFull: %w", string(b), err)
	}

	if len(vListString) > 1 {
		if vListString[1] == "" {
			vr.End = XenVersionMaster
		} else {
			if end, err := XenVersionFromString(vListString[1]); err == nil {
				// Try parsing as minor release ("a.b")
				vr.End = end
			} else if endFull, err := XenVersionFullFromString(vListString[1]); err == nil {
				// Try parsing it as a point release ("a.b.c")
				vr.End = endFull.XenVersion()
			} else {
				return fmt.Errorf("VersionRange: Parsing second value as a XenVersion: %v", err)
			}
		}
	} else {
		switch vr.Start {
		case XenVersionMin:
			vr.End = XenVersionMaster
		default:
			vr.End = vr.Start
		}
	}

	if vr.Start.IsGreaterThan(vr.End) {
		return fmt.Errorf("VersionRange: Start %v greater End %v",
			vr.Start, vr.End)
	}

	return nil
}

type XenVersionFullRange struct {
	Start XenVersionFull
	End   XenVersionFull
}

// Check to see if the range is valid.  Ranges are valid if
//
// 1. All contaned versions are valid
//
// 2. All versions are on the same 'branch'.
//
// 3. fvr.End >= fvr.Start
//
// Versions can be either on release branches (i.e., '4.13.1' and
// '4.13.2') or the master branch (i.e., '4.14.0' and '4.15.0').
//
// XenVersionFullMaster counts as a point on any branch.
func (fvr *XenVersionFullRange) IsValid() bool {
	if !fvr.Start.Check() || !fvr.End.Check() {
		return false
	}
	if !(fvr.End == XenVersionFullMaster ||
		fvr.Start.XenVersion() == fvr.End.XenVersion() ||
		fvr.Start.Point() == 0 && fvr.End.Point() == 0) {
		return false
	}
	return !fvr.Start.IsGreaterThan(fvr.End)
}

// Merge range fvrb into fvra.  fvra may be empty, or have both values
// set to XenVersionFullInvalid; if so, fvrb is copied into fvra.
//
// If fvra is not empty, fvra and fvrb must be on the same 'branch'.
// See XenVersionFullRange.IsValid() for a description of branches.
func (fvra *XenVersionFullRange) Merge(fvrb XenVersionFullRange) error {
	// fvra is empty; just copy fvrb in.
	if (fvra.Start == "" && fvra.End == "") ||
		(fvra.Start == XenVersionFullInvalid && fvra.End == XenVersionFullInvalid) {
		fvra.Start = fvrb.Start
		fvra.End = fvrb.End
		return nil
	}

	if !fvra.IsValid() {
		return fmt.Errorf("Invalid merge accumulator: %v", fvra)
	}

	if !fvrb.IsValid() {
		return fmt.Errorf("Invalid merge input: %v", fvrb)
	}

	// Do a and b have 'master-compatible' versions?
	aMaster := fvra.Start.Point() == 0 &&
		(fvra.End.Point() == 0 || fvra.End == XenVersionFullMaster)
	bMaster := fvrb.Start.Point() == 0 &&
		(fvrb.End.Point() == 0 || fvrb.End == XenVersionFullMaster)

	if !(aMaster && bMaster) && fvra.Start.XenVersion() != fvrb.Start.XenVersion() {
		return fmt.Errorf("Attempting to merge incompatible Xen Versions %v and %v",
			fvra.Start.XenVersion(), fvrb.Start.XenVersion())
	}

	if fvra.Start.IsGreaterThan(fvrb.Start) {
		fvra.Start = fvrb.Start
	}

	if fvrb.End.IsGreaterThan(fvra.End) {
		fvra.End = fvrb.End
	}

	return nil
}
