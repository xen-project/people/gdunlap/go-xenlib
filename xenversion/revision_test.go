package xenversion

import (
	"testing"
)

func TestPointRelease(t *testing.T) {
	tests := []struct {
		inv     XenVersion
		inpoint int
		want    XenVersionFull
	}{
		{XenVersionMaster, 0, XenVersionFullInvalid},
		{XenVersionMin, 0, XenVersionFullInvalid},
		{XenVersionInvalid, 0, XenVersionFullInvalid},
		{XenVersion("4.14"), 0, XenVersionFull("4.14.0")},
		{XenVersion("4.13"), 1, XenVersionFull("4.13.1")},
		{XenVersion("4.12"), 2, XenVersionFull("4.12.2")},
	}

	for _, test := range tests {
		t.Logf("Test case %v", test)
		got := test.inv.PointRelease(test.inpoint)
		if got != test.want {
			t.Errorf("Got %v, wanted %v!", got, test.want)
			return
		}
	}
}
