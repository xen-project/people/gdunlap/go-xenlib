package xenversion

import "fmt"

// StableBranch returns the stable branch name for the given
// XenVersion, as a string: i.e., stable-M.mm.  For XenVersionMaster,
// it returns "master"; for XenVersionMin, it returns "INVALID".
func (v XenVersion) StableBranch() string {
	switch v {
	case XenVersionMaster:
		return "master"
	case XenVersionMin, XenVersionInvalid:
		return invString
	default:
		return "stable-" + v.String()
	}
}

// StableBranch returns the stable branch name for the given
// XenVersion, as a string: i.e., staging-M.mm.  For XenVersionMaster,
// it returns "staging"; for XenVersionMin, it returns "INVALID".
func (v XenVersion) StagingBranch() string {
	switch v {
	case XenVersionMaster:
		return "staging"
	case XenVersionMin, XenVersionInvalid:
		return invString
	default:
		return "staging-" + v.String()
	}
}

// StableBranch returns the release tag for the given XenVersionFull,
// as a string: i.e., RELEASE-M.mm.  For XenVersionMaster and
// XenVersionMin, it returns "INVALID".
func (fv XenVersionFull) ReleaseTag() string {
	switch fv {
	case XenVersionFullMaster, XenVersionFullMin, XenVersionFullInvalid:
		return invString
	default:
		return "RELEASE-" + fv.String()
	}
}

// PointRelease returns a full version made up of the minor version at
// the specified point release.
func (v XenVersion) PointRelease(point int) XenVersionFull {
	switch v {
	case XenVersionMaster, XenVersionMin, XenVersionInvalid:
		return XenVersionFullInvalid
	default:
		return XenVersionFull(fmt.Sprintf("%s.%d", v, point))
	}
}
