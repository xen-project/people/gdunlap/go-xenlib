package xenversion

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
)

type XenVersion string

// XenVersionMaster is always the 'maximum'
const masterString = "master"
const minString = "MIN"
const invString = "INVALID"
const XenVersionMaster = XenVersion(masterString)
const XenVersionMin = XenVersion(minString)
const XenVersionInvalid = XenVersion(invString)
const XenVersionFullMaster = XenVersionFull(masterString)
const XenVersionFullMin = XenVersionFull(minString)
const XenVersionFullInvalid = XenVersionFull(invString)

// Alternate acceptable names for "master"
var masterStrings = []string{"master", "xen-unstable"}

func isMasterString(s string) bool {
	for i := range masterStrings {
		if s == masterStrings[i] {
			return true
		}
	}
	return false
}

func isMinString(s string) bool {
	return s == minString
}

// NB if Xen's major ever exceeds 2 billion-1, we'll
// need to deprecate the 32-bit build of this tool.

// xenVersionMasterMajor is the sentinel value used for "Master"
const xenVersionMasterMajor = math.MaxInt32

// This is a bit weird but it hides the implementation
func (v XenVersion) String() string {
	return string(v)
}

var versionRegexpList = []*regexp.Regexp{
	regexp.MustCompile(`^([0-9]+)\.([0-9]+)$`),
	regexp.MustCompile(`^([0-9]+)\.([0-9]+).x[.]?$`), // Tolerate XSA-440's stray `.`
	regexp.MustCompile(`^([0-9]+)\.([0-9]+)-stable$`),
}

func (v *XenVersion) parse() (major, minor int, err error) {
	switch {
	case *v == XenVersionMin:
		return -1, 0, nil
	case *v == XenVersionMaster,
		regexp.MustCompile(`^[0-9]+\.[0-9]+-rc[0-9]*$`).MatchString(string(*v)):
		return xenVersionMasterMajor, 0, nil
	case isMinString(string(*v)):
		*v = XenVersionMin
		return -1, 0, nil
	case isMasterString(string(*v)):
		*v = XenVersionMaster
		return xenVersionMasterMajor, 0, nil
	}

	major = -1
	minor = -1

	var submatch []string
	for _, re := range versionRegexpList {
		submatch = re.FindStringSubmatch(string(*v))
		if submatch != nil {
			break
		}
	}

	if submatch == nil {
		err = fmt.Errorf("Bad XenVersion: '%s'", string(*v))
		return
	}

	major, err = strconv.Atoi(submatch[1])
	if err != nil {
		return
	}

	if major >= xenVersionMasterMajor {
		err = fmt.Errorf("Version too large")
		major = -1
		return
	}

	// Canonicalize this version
	*v = XenVersion(submatch[1] + "." + submatch[2])

	minor, err = strconv.Atoi(submatch[2])

	return
}

func (v XenVersion) Check() bool {
	_, _, err := v.parse()

	return err == nil
}

func XenVersionFromString(s string) (v XenVersion, err error) {
	v = XenVersion(s)
	_, _, err = v.parse()
	if err != nil {
		v = ""
	}
	return
}

func (v XenVersion) gt(v2 XenVersion, orEqual bool) bool {
	majorA, minorA, err := v.parse()
	if err != nil {
		fmt.Printf("Parsing: %v\n", err)
		panic("Unexpected error")
	}

	majorB, minorB, err := v2.parse()
	if err != nil {
		fmt.Printf("Parsing: %v\n", err)
		panic("Unexpected error")
	}

	if majorA > majorB {
		return true
	}

	if majorA < majorB {
		return false
	}

	// Now we know majors are equal

	if minorA > minorB {
		return true
	}

	if orEqual && minorA == minorB {
		return true
	}

	return false
}

// a.IsGreaterEqualThan(b) returns a >= b
func (v XenVersion) IsGreaterEqualThan(v2 XenVersion) bool {
	return v.gt(v2, true)
}

// a.IsGreaterEqualThan(b) returns a > b
func (v XenVersion) IsGreaterThan(v2 XenVersion) bool {
	return v.gt(v2, false)
}

func (v XenVersion) PrevVersiongit() (next XenVersion, err error) {
	major, minor, err := v.parse()
	if err != nil {
		err = fmt.Errorf("Parsing version %v: %v\n", v, err)
		return
	}

	if minor > 0 {
		minor--
		next = XenVersion(fmt.Sprintf("%d.%d\n", major, minor))
		return
	}

	// FIXME If we ever move to 5.x, or need to extend this to
	// 3.x, we'll have to sort something out here; probably a
	// table mapping major version -> highest release minor
	err = fmt.Errorf("Don't know how to roll back major number")
	return
}

type XenVersionSlice []XenVersion

// Len is the number of elements in the collection.
func (vl XenVersionSlice) Len() int {
	return len(vl)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (vl XenVersionSlice) Less(i, j int) bool {
	return vl[i].IsGreaterThan(vl[j])
}

// Swap swaps the elements with indexes i and j.
func (vl XenVersionSlice) Swap(i, j int) {
	tmp := vl[j]
	vl[j] = vl[i]
	vl[i] = tmp
}

func (vl XenVersionSlice) Present(v XenVersion) (found bool) {
	for _, cv := range vl {
		if cv == v {
			found = true
			break
		}
	}
	return
}

func (vl *XenVersionSlice) Truncate(n int) {
	*vl = (*vl)[:n]
}

func (a XenVersionSlice) IsEqual(b XenVersionSlice) bool {
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

// NB that this list goes high to low
// all: All versions in the current xsa (or all with patches)
// <v>: One particular version
// <v1>..<v2>: All versions between <v1> and <v2>
// <v>.. : All versions newer than <v>
// ..<v> : All versions older than <v>
// TODO: Maybe add ','?
func VersionsFromString(s string, validVersions XenVersionSlice, filter func(v XenVersion) bool) (vl XenVersionSlice, err error) {
	// If it parses as a single version, just pass that back.
	v := XenVersion(s)

	if _, _, terr := v.parse(); terr == nil {
		if validVersions != nil && !validVersions.Present(v) {
			err = fmt.Errorf("Version %v not listed", v)
			return
		}

		if filter != nil && !filter(v) {
			err = fmt.Errorf("Only value filtered")
			return
		}

		vl = XenVersionSlice{v}
		return
	}

	vmin := XenVersionMin
	vmax := XenVersionMaster

	if s != "all" {
		submatch := regexp.MustCompile(`^([0-9.]*|master)\.\.([0-9.]*)$`).FindStringSubmatch(s)
		if len(submatch) != 3 {
			err = fmt.Errorf("Cannot parse version list")
			return
		}

		if submatch[1] != "" {
			vmax = XenVersion(submatch[1])
		}

		if submatch[2] != "" {
			vmin = XenVersion(submatch[2])
		}

		if !vmin.Check() {
			err = fmt.Errorf("Invalid version: %s", vmin)
			return
		}

		if !vmax.Check() {
			err = fmt.Errorf("Invalid version: %s", vmax)
			return
		}
	}

	if !vmax.IsGreaterEqualThan(vmin) {
		err = fmt.Errorf("Range max %v less than min %v", vmax, vmin)
		return
	}

	// We have a list of valid versions, so just go through the list picking
	// out the valid ones
	if validVersions != nil {
		if vmin != XenVersionMin && !validVersions.Present(vmin) {
			err = fmt.Errorf("Version %v not listed", vmin)
			return
		}
		if vmax != XenVersionMaster && !validVersions.Present(vmax) {
			err = fmt.Errorf("Version %v not listed", vmax)
			return
		}

		for _, cv := range validVersions {
			// FIXME: Should a filter failure cause a 'break' instead?
			if cv.IsGreaterEqualThan(vmin) &&
				vmax.IsGreaterEqualThan(cv) &&
				(filter == nil || filter(cv)) {
				vl = append(vl, cv)
			}
		}

		return
	}

	// We don't have any users w/o valid versions yet; implement
	// this when we need to
	err = fmt.Errorf("INTERNAL ERROR: Parsing w/o valid versions not implemented")

	return
}

type XenVersionFull string

func (v XenVersionFull) String() string {
	return string(v)
}

func (v XenVersionFull) parse() (ver XenVersion, p int, err error) {
	switch {
	case isMinString(string(v)):
		return XenVersionMin, 0, nil
	case isMasterString(string(v)):
		return XenVersionMaster, 0, nil
	}

	submatch := regexp.MustCompile("^([0-9]+.[0-9]+).([0-9]+)$").FindStringSubmatch(string(v))

	if len(submatch) != 3 {
		err = fmt.Errorf("Bad XenVersionFull: %s", string(v))
		return
	}

	ver = XenVersion(submatch[1])
	p, err = strconv.Atoi(submatch[2])

	return
}

func (v XenVersionFull) Check() bool {
	_, _, err := v.parse()

	return (err == nil)
}

func XenVersionFullFromString(s string) (fv XenVersionFull, err error) {
	fv = XenVersionFull(s)
	_, _, err = fv.parse()
	if err != nil {
		fv = ""
	}
	return
}

func (v XenVersionFull) XenVersion() XenVersion {
	ver, _, err := v.parse()
	if err != nil {
		return XenVersion("Error: Couldn't parse full version " + string(v))
	}
	return ver
}

func (v XenVersionFull) Point() int {
	_, point, err := v.parse()
	if err != nil {
		return -1
	}
	return point
}

type XenVersionFullSlice []XenVersionFull

// a.IsGreaterEqualThan(b) returns a > b
func (a XenVersionFull) IsGreaterThan(b XenVersionFull) bool {
	va := a.XenVersion()
	vb := b.XenVersion()
	switch {
	case va.IsGreaterThan(vb):
		return true
	case vb.IsGreaterThan(va):
		return false
	}

	ap := a.Point()
	bp := b.Point()

	return ap > bp
}

// Len is the number of elements in the collection.
func (fvl XenVersionFullSlice) Len() int {
	return len(fvl)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (fvl XenVersionFullSlice) Less(i, j int) bool {
	return fvl[i].IsGreaterThan(fvl[j])
}

// Swap swaps the elements with indexes i and j.
func (fvl XenVersionFullSlice) Swap(i, j int) {
	tmp := fvl[j]
	fvl[j] = fvl[i]
	fvl[i] = tmp
}

func (fvl XenVersionFullSlice) Present(v XenVersionFull) (found bool) {
	for _, cv := range fvl {
		if cv == v {
			found = true
			break
		}
	}
	return
}

func (fvl *XenVersionFullSlice) Truncate(n int) {
	*fvl = (*fvl)[:n]
}

func (a XenVersionFullSlice) IsEqual(b XenVersionFullSlice) bool {
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
