package repocache

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/go-git/go-git/v5"
)

// TODO: Add some way to specify an alternate location for the cache

var cachedir string

type CachedRepository struct {
	*git.Repository
	origin string
}

type openOptionStruct struct {
	updateOnOpen bool
}

type OpenOption func(opts *openOptionStruct) error

func OpenUpdate() OpenOption {
	return func(opts *openOptionStruct) error {
		opts.updateOnOpen = true
		return nil
	}
}

func initCacheDir() error {
	var err error
	cachedir, err = os.UserCacheDir()
	if err != nil {
		return fmt.Errorf("Getting system cache directory: %v", err)
	}

	cachedir += "/repocache"

	// Just check to make sure we have read access to the directory
	_, err = ioutil.ReadDir(cachedir)
	if os.IsNotExist(err) {
		err := os.Mkdir(cachedir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("Creating repocache directory %s: %v",
				cachedir, err)
		}
	} else if err != nil {
		return fmt.Errorf("Reading cache directory %s: %v",
			cachedir, err)
	}

	return nil
}

func originToPath(origin string) string {
	return cachedir + "/" + origin
}

func Open(origin string, opts ...OpenOption) (*CachedRepository, error) {
	var opt openOptionStruct
	for _, f := range opts {
		if err := f(&opt); err != nil {
			return nil, err
		}
	}

	if err := initCacheDir(); err != nil {
		return nil, err
	}

	var err error
	repo := &CachedRepository{origin: origin}

	repopath := originToPath(origin)
	repourl := "https://" + origin

	// Try plain open
	repo.Repository, err = git.PlainOpen(repopath)
	if err == git.ErrRepositoryNotExists {
		// Repository doesn't exist; try cloning it
		log.Printf("Cloning %s into %s", repourl, repopath)
		repo.Repository, err = git.PlainClone(repopath, true,
			&git.CloneOptions{URL: repourl})
		if err != nil {
			return nil, fmt.Errorf("Cloning url %s: %v", repourl, err)
		}
	} else if err != nil {
		// Return an error
		return nil, fmt.Errorf("Opening repopath %s: %v", repopath, err)
	} else if opt.updateOnOpen {
		err := repo.Update()
		if err != nil {
			return nil, err
		}
	}

	return repo, nil
}

func (r *CachedRepository) Update() error {
	log.Printf("Updating repository at %s...\n", originToPath(r.origin))
	err := r.Fetch(&git.FetchOptions{Force: true})
	if err != nil && err != git.NoErrAlreadyUpToDate {
		return fmt.Errorf("Updating repo: %v", err)
	}
	return nil
}
