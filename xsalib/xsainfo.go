package xsalib

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	//"sort"
	"time"

	"github.com/bluekeyes/go-gitdiff/gitdiff"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/mpvl/unique"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

var XSAURLRoot = "https://xenbits.xenproject.org/xsa"

var XSAJson = "xsa.json"

// "Versions" is unprocessed information from the raw data set.
// XenVersionList is the same data, but processed into a list of
// specific point releases, based on what was available at the time
// the XSA was publicly released.  (And is only filled out if Project
// == "xen".)
type XSAFileInfo struct {
	Name           string
	Project        string
	Versions       []xenversion.XenVersionRange
	XenVersionList xenversion.XenVersionSlice

	xsa      *XSAInfo // Up-pointer to the  main XSA struct
	contents []byte   // Contents of the file
	message  string   // The commit message, when parsed as a patch

	// "Cache" of potential commits matching this patch for a given Xen version
	commits map[xenversion.XenVersion][]*object.Commit
}

func (xfi *XSAFileInfo) NewReader() (*bytes.Reader, error) {
	if xfi.contents == nil {
		var source *xsaSource

		if xfi.xsa != nil {
			source = xfi.xsa.source
		}

		if source == nil {
			return nil, fmt.Errorf("No source available")
		}

		if source.stype != sourceWeb {
			return nil, fmt.Errorf("File fetch for sourcetype %d not implemented", source.stype)
		}

		var err error

		// FetchURL returns nil bytes on error; no need for a local variable here
		xfi.contents, err = source.FetchURL(xfi.Name)

		if err != nil {
			return nil, fmt.Errorf("Fetching data: %v", err)
		}
	}

	return bytes.NewReader(xfi.contents), nil
}

func (xfi *XSAFileInfo) Message() (string, error) {
	if xfi.message == "" {
		r, err := xfi.NewReader()
		if err != nil {
			return "", fmt.Errorf("Getting reader: %v", err)
		}

		_, preamble, err := gitdiff.Parse(r)
		if err != nil {
			return "", fmt.Errorf("Parsing patch: %v", err)
		}

		header, err := gitdiff.ParsePatchHeader(preamble)
		if err != nil {
			return "", fmt.Errorf("Parsing patch header:  %v", err)
		}

		// header.Message tends to remove the final linefeed
		xfi.message = header.Message() + "\n"
	}

	return xfi.message, nil
}

type XSATime struct {
	time.Time
}

const XSATimeFormat = `"2006-01-02 15:04"`

func (xt *XSATime) UnmarshalJSON(b []byte) error {
	var err error
	xt.Time, err = time.Parse(XSATimeFormat, string(b))
	return err
}

// XSAInfo contains information about a given XSA.
//
// XenVersionList and ProjectList are "accumulated" values from Files,
// to make it easier to filter XSAs at the top level.
type XSAInfo struct {
	Cve         []string
	Title       string
	PublicTime  XSATime `json:"public_time"`
	VersionTime XSATime `json:"version_time"`
	Version     int     // Version of the XSA
	Files       []XSAFileInfo
	XSA         int `json:",string"` // For some reason this is a string rather than an integer
	Deallocated bool
	Withdrawn   bool

	XenVersionList xenversion.XenVersionSlice
	ProjectList    []string

	source *xsaSource // Where should we look to get the contents of the file?
}

type FileFilter func(*XSAFileInfo) bool

// Filter out files which are not of the particular Xen version
func FileFilterVersion(v xenversion.XenVersion) FileFilter {
	return func(fi *XSAFileInfo) bool {
		return fi.Project != "xen" || !fi.XenVersionList.Present(v)
	}
}

func (xi *XSAInfo) FilterFiles(filters ...FileFilter) []XSAFileInfo {
	files := []XSAFileInfo{}

fileloop:
	for i := range xi.Files {
		fi := &xi.Files[i]
		for j := range filters {
			if filters[j](fi) {
				continue fileloop
			}
		}
		files = append(files, *fi)
	}

	return files
}

// Return whether the XSA has any files related to the given project
func (xi *XSAInfo) HasProject(project string) bool {
	for i := range xi.ProjectList {
		if xi.ProjectList[i] == project {
			return true
		}
	}
	return false
}

type XSAList []XSAInfo

const (
	sourceWeb = iota
	sourceBytes
	sourceXSAGit
)

type xsaSource struct {
	stype    int
	location string
}

func (s xsaSource) GetURL(rpath string) string {
	if s.stype != sourceWeb {
		panic("GetURL called on inappropriate source type")
	}
	return s.location + "/" + rpath
}

func (s xsaSource) FetchURL(rpath string) ([]byte, error) {
	var Client http.Client

	url := s.GetURL(rpath)

	resp, err := Client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Fetching URL %s: %v", url, err)
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Fetching URL %s: Status code %d", url, resp.StatusCode)
	}

	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Reading response body: %v", err)
	}

	return b, nil
}

type getXSAInfoOpt struct {
	source             *xsaSource
	b                  []byte
	preprocessfilters  []func(xsa *XSAInfo) bool
	postprocessfilters []func(xsa *XSAInfo) bool
}

// These functions are used to pass options into XSAInfoOpt
type XSAInfoOpt func(*getXSAInfoOpt) error

func (opt *getXSAInfoOpt) getXSAURL() error {
	var err error
	opt.b, err = opt.source.FetchURL(XSAJson)
	if err != nil {
		return err
	}

	return nil
}

// Use b as a source for the XSA info JSON
func Bytes(b []byte) XSAInfoOpt {
	return func(opt *getXSAInfoOpt) error {
		opt.source.stype = sourceBytes
		opt.b = b
		return nil
	}
}

// Add a filter which only passes through XSAs which have been
// released since the given branch's last point release.  This is
// *only* a time-based filter; it will not filter out XSAs with no
// files for this particular release.
//
// NB that the "last point release" for "master" is the most recent .0
// release.  Also NB that stacked filters are intersections, not
// unions.
func FilterVersionBranchSince(v xenversion.XenVersion) XSAInfoOpt {
	fv, err := xeninfo.LatestPointRelease(v)
	if err != nil {
		// WOW
		return func(opt *getXSAInfoOpt) error {
			return fmt.Errorf("Getting latest point release for version %v: %v",
				v, err)
		}
	}
	fvi, err := xeninfo.GetVersionFullInfo(fv)
	if err != nil {
		return func(opt *getXSAInfoOpt) error {
			return fmt.Errorf("Getting info for full version %v: %v",
				v, err)
		}
	}

	return func(opt *getXSAInfoOpt) error {
		opt.preprocessfilters = append(opt.preprocessfilters, func(xsa *XSAInfo) bool {
			// Skip if the XSA is deallocated, or went public before the named time
			if xsa.Deallocated {
				return true
			}
			return xsa.PublicTime.Before(fvi.Time)
		})
		// // Vestigal code
		// opt.postprocessfilters = append(opt.postprocessfilters, func(xsa *XSAInfo) bool {
		// 	// Skip if the version isn't listed in any files labeled for the Xen project
		// 	return !xsa.XenVersionList.Present(v)
		// })
		return nil
	}
}

// Add a filter which only matches the single XSA number
func FilterXSANum(xsanum int) XSAInfoOpt {
	return func(opt *getXSAInfoOpt) error {
		opt.preprocessfilters = append(opt.preprocessfilters, func(xsa *XSAInfo) bool {
			return xsa.XSA != xsanum
		})
		return nil
	}
}

// Filter out XSAs with PublicTime before a given time
func FilterBefore(ts time.Time) XSAInfoOpt {
	return func(opt *getXSAInfoOpt) error {
		opt.preprocessfilters = append(opt.preprocessfilters, func(xsa *XSAInfo) bool {
			return xsa.PublicTime.Before(ts)
		})
		return nil
	}
}

// Filter out XSAs with PublicTime after a given time
func FilterAfter(ts time.Time) XSAInfoOpt {
	return func(opt *getXSAInfoOpt) error {
		opt.preprocessfilters = append(opt.preprocessfilters, func(xsa *XSAInfo) bool {
			return xsa.PublicTime.After(ts)
		})
		return nil
	}
}

// Filter out deallocated XSAs
func FilterDeallocated() XSAInfoOpt {
	return func(opt *getXSAInfoOpt) error {
		opt.preprocessfilters = append(opt.preprocessfilters, func(xsa *XSAInfo) bool {
			return xsa.Deallocated
		})
		return nil
	}
}

func GetXSAInfo(options ...XSAInfoOpt) (XSAList, error) {
	opt := getXSAInfoOpt{source: &xsaSource{stype: sourceWeb, location: XSAURLRoot}}

	for _, option := range options {
		err := option(&opt)
		if err != nil {
			return nil, fmt.Errorf("Parsing options: %v", err)
		}
	}

	switch opt.source.stype {
	case sourceWeb:
		err := opt.getXSAURL()
		if err != nil {
			return nil, err
		}
	case sourceBytes:
		break
	default:
		return nil, fmt.Errorf("Not implemented")
	}

	var XSAJSONStruct []struct{ XSAS XSAList } // For some reason, there's an extra two layers here...
	err := json.Unmarshal(opt.b, &XSAJSONStruct)
	if err != nil {
		return nil, fmt.Errorf("Unmarshaling JSON: %v", err)
	}

	allversions, err := xeninfo.VersionInfoList()
	if err != nil {
		return nil, fmt.Errorf("Getting all versions: %v", err)
	}

	xsas := XSAJSONStruct[0].XSAS

	// We filter-in-place, collapsing down the slice and returning a truncated version
	j := 0
	filterPresent := !(opt.preprocessfilters == nil && opt.postprocessfilters == nil)

xsaloop:
	for i := range xsas {
		xsa := &xsas[i]

		for _, f := range opt.preprocessfilters {
			if f(xsa) {
				continue xsaloop
			}
		}

		xsa.source = opt.source

		xsaVersions := []xenversion.XenVersion{}

		xsaProjects := []string{}

		for j := range xsa.Files {
			file := &xsa.Files[j]

			file.xsa = xsa
			file.commits = make(map[xenversion.XenVersion][]*object.Commit)

			if file.Project != "" {
				xsaProjects = append(xsaProjects, file.Project)
			}

			if file.Project != "xen" {
				continue
			}

			fileVersions := []xenversion.XenVersion{}

			for k := range file.Versions {
				vs := xeninfo.ExpandVersionRange(file.Versions[k],
					xsa.PublicTime.Time,
					allversions)
				fileVersions = append(fileVersions, vs...)
				xsaVersions = append(xsaVersions, vs...)
			}

			// unique.Sort will sort and then uniqify
			unique.Sort((*xenversion.XenVersionSlice)(&fileVersions))

			file.XenVersionList = fileVersions
		}

		// Uniqify accumulated XenVersions and projects and store them
		// in the toplevel struct. NB that unique.Sort will sort and
		// then uniqify.
		unique.Sort((*xenversion.XenVersionSlice)(&xsaVersions))
		unique.Sort(unique.StringSlice{P: &xsaProjects})

		xsa.XenVersionList = xsaVersions
		xsa.ProjectList = xsaProjects

		for _, f := range opt.postprocessfilters {
			if f(xsa) {
				continue xsaloop
			}
		}

		if filterPresent {
			xsas[j] = *xsa
			j++
		}
	}

	switch {
	case !filterPresent:
		return xsas, nil
	case j == 0:
		return nil, nil
	default:
		return xsas[:j], nil
	}
}
