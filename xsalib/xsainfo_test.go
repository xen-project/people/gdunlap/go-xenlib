package xsalib

import (
	"io"
	"testing"
)

func TestFetchAndParse(t *testing.T) {
	xsalist, err := GetXSAInfo()
	if err != nil {
		t.Errorf("GetXSAInfo: %v", err)
		return
	}

	if len(xsalist) == 0 {
		t.Errorf("Empty XSAlist!")
		return
	}

	t.Logf("First XSA number: %d\nLast XSA number: %d\n",
		xsalist[0].XSA,
		xsalist[len(xsalist)-1].XSA)

	t.Logf("%v\n%v\n",
		xsalist[0],
		xsalist[len(xsalist)-1])

	var i int
	for i = len(xsalist) - 1; i > 0; i-- {
		if len(xsalist[i].Files) > 0 {
			break
		}
	}

	xfi := xsalist[i].Files[0]

	r, err := xfi.NewReader()
	if err != nil {
		t.Errorf("Error getting newreader for xsa %d file %s: %v",
			xsalist[i].XSA, xfi.Name, err)
		return
	}

	b, err := io.ReadAll(r)
	if err != nil {
		t.Errorf("Reading from reader: %v", err)
		return
	}

	t.Logf("XSA %d file %s, got %d bytes",
		xsalist[i].XSA, xfi.Name, len(b))
}
