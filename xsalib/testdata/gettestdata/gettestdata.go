package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	"github.com/samber/lo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib/testdata"
)

// Idea: Generate a list of patches (i.e., the content), minor version, and commit hash.
// Walk from the tip of stable-$version back to its merge-base with master,
// checking that the patch matches with the given commit hash, and no others.

var skip = map[string]bool{
	// It was specified in the advisory that XSA-403 patch 2 was not
	// going to be applied to the tree.
	"XSA-403 @ master : xsa403/xsa403-2.patch":    true,
	"XSA-403 @ 4.16 : xsa403/xsa403-4.16-2.patch": true,
	"XSA-403 @ 4.15 : xsa403/xsa403-4.16-2.patch": true,
	"XSA-403 @ 4.14 : xsa403/xsa403-4.14-2.patch": true,
	"XSA-403 @ 4.13 : xsa403/xsa403-4.14-2.patch": true,
	// xsa380-3.patch appears to have been missed for 4.11 and 4.12
	"XSA-380 @ 4.12 : xsa380/xsa380-3.patch": true,
	"XSA-380 @ 4.11 : xsa380/xsa380-3.patch": true,
	// xsa384-4.11.patch appears to have been missed
	"XSA-384 @ 4.11 : xsa384-4.11.patch": true,
}

func main() {

	var ctcs []testdata.CommitTestCase
	var missing []testdata.CommitTestCase

	log.Print("Getting XSA information")
	xsas, err := xsalib.GetXSAInfo(xsalib.FilterBefore(time.Date(2020, 12, 1, 12, 0, 0, 0, time.UTC)))
	if err != nil {
		log.Fatalf("Getting XSA info: %v", err)
	}

	var allversions xenversion.XenVersionSlice
	for i := range xsas {
		allversions = lo.Union(allversions, xsas[i].XenVersionList)
	}

	sort.Sort(allversions)

	log.Printf("Versions represented in XSA list: %v", allversions)

	for _, v := range allversions {
		log.Printf("Doing batch commit search for version %v...", v)
		if err := xsas.CommitBatchSearch(v); err != nil {
			log.Fatalf("Doing batch commit search: %v", err)
		}
	}

	for i := range xsas {
		xsa := &xsas[i]
		log.Printf("Processing xsa %d...", xsa.XSA)

		for _, file := range xsa.Files {
			if file.Project != "xen" {
				continue
			}

			log.Printf("Processing file %s...", file.Name)

			for _, v := range file.XenVersionList {
				ctc := testdata.CommitTestCase{
					Label:   fmt.Sprintf("XSA-%d @ %v : %s", xsa.XSA, v, file.Name),
					Version: v,
				}

				if skip[ctc.Label] {
					log.Printf("Skipping patch %s", ctc.Label)
					continue
				}

				log.Printf("Creating testcase %s", ctc.Label)

				ctc.CommitMessage, err = file.Message()
				if err != nil {
					log.Fatalf("Getting commit message for file %s: %v", file.Name, err)
				}

				cs, err := file.Commits(v)
				if err != nil {
					log.Fatalf("Getting commits for file %s: %v", file.Name, err)
				}

				if len(cs) == 1 {
					ctc.Hash = cs[0].Hash.String()
				} else {
					missing = append(missing, ctc)
				}

				ctcs = append(ctcs, ctc)
			}
		}
	}

	out, err := json.MarshalIndent(testdata.CommitTestInfoStruct{Versions: allversions, TestCases: ctcs}, "", " ")
	if err != nil {
		log.Fatalf("Marshalling commit list: %v", err)
	}

	ofname := "../CommitTestData.json"

	f, err := os.Create(ofname)
	if err != nil {
		log.Fatalf("Opening output file: %v", err)
	}

	if _, err := f.Write(out); err != nil {
		log.Fatalf("Writing output file: %v", err)
	}

	log.Printf("Commit data written to %s", ofname)

	if len(missing) > 0 {
		log.Printf("WARNING: %d missing changesets", len(missing))
		for _, ctc := range missing {
			log.Printf(" %s (%s)", ctc.Label, ctc.CommitMessage[:40])
		}
	}
}
