package testdata

import (
	"encoding/json"
	"fmt"

	_ "embed"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

type CommitTestCase struct {
	Label         string // "XSA-%d @ %v : %s", xsa.XSA, Version, file.Name
	CommitMessage string
	Version       xenversion.XenVersion
	Hash          string // So that the marshalled version is human readable / editable
}

type CommitTestInfoStruct struct {
	Versions  xenversion.XenVersionSlice
	TestCases []CommitTestCase
}

//go:embed CommitTestData.json
var RawTestData []byte

var CommitTestInfo CommitTestInfoStruct

func Init() error {
	if err := json.Unmarshal(RawTestData, &CommitTestInfo); err != nil {
		return fmt.Errorf("Unmarshaling data: %w", err)
	}

	return nil
}
