package xsalib

import (
	"testing"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

func TestFilterBranch(t *testing.T) {
	{
		// Test the filter on 4.8.  This has the disadvantage that
		// some of the versioning information is unavailable to do not
		// parsing legacy formatting; but has the advantage that it
		// should not change any more, so we can define the "right" answer.
		v := xenversion.XenVersion("4.8")

		vis, err := xeninfo.VersionInfoList(xeninfo.FilterXenVersion(v))
		if err != nil {
			t.Errorf("Unexpected error getting versionlist for version %v: %v",
				v, err)
			return
		}

		if len(vis) != 1 {
			t.Errorf("Unexpected number of versions: got %v, wanted 1", len(vis))
			return
		}

		want := []int{
			283, 284, 285, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296,
			297, 298, 299, 300, 301, 302, 303, 304, 305, 306}

		xsas, err := GetXSAInfo(FilterVersionBranchSince(xenversion.XenVersion("4.8")), FilterAfter(vis[0].SecuritySupportUntil))
		if err != nil {
			t.Errorf("Getting list filtered for 4.8: %v", err)
			return
		}
		t.Logf("Found %d uncommitted XSAs which should be on stable-4.8", len(xsas))

		got := []int{}

		for _, xsa := range xsas {
			got = append(got, xsa.XSA)
		}

		if len(want) != len(got) {
			t.Errorf("Error, wanted %v, got %v", want, got)
			return
		}

		for i := range want {
			if want[i] != got[i] {
				t.Errorf("Error, wanted %v, got %v", want, got)
				return
			}
		}
	}

}

func TestFilterXSANum(t *testing.T) {
	xsanum := 327

	xsas, err := GetXSAInfo(FilterXSANum(xsanum))
	if err != nil {
		t.Errorf("Getting XSA 'list' filtered on XSA %d: %v", xsanum, err)
		return
	}

	if len(xsas) != 1 {
		t.Logf("Expecting a single XSA, got %d!", len(xsas))
		return
	}

	if xsas[0].XSA != xsanum {
		t.Logf("Expected XSA number %d, got %d!", xsanum, xsas[0].XSA)
		return
	}
}
