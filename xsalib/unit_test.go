package xsalib

import (
	"testing"

	"time"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

type instance struct {
	b    string
	XSAS []XSAInfo
}

var inputs = []instance{
	{
		`[{"xsas": [{"xsa":"26","files":[{"name":"xsa26-4.1.patch"},{"name":"xsa26-4.2.patch"},{"name":"xsa26-unstable.patch"}],"public_time":"2012-12-03 17:51","version_time":"2012-12-03 17:51","cve":["CVE-2012-5510"],"title":"Grant table version switch list corruption vulnerability","version":3}]}]`,
		[]XSAInfo{{
			XSA:         26,
			Cve:         []string{"CVE-2012-5510"},
			Title:       "Grant table version switch list corruption vulnerability",
			PublicTime:  XSATime{Time: time.Date(2012, 12, 3, 17, 51, 0, 0, time.UTC)},
			VersionTime: XSATime{Time: time.Date(2012, 12, 3, 17, 51, 0, 0, time.UTC)},
			Version:     3,
			Files: []XSAFileInfo{
				{Name: "xsa26-4.1.patch"},
				{Name: "xsa26-4.2.patch"},
				{Name: "xsa26-unstable.patch"},
			},
		}},
	},
	{`[{"xsas": [{"files":[{"name":"xsa328.meta"},{"name":"xsa328/xsa328-1.patch","project":"xen","versions":[["xen-unstable"]]},{"name":"xsa328/xsa328-2.patch","project":"xen","versions":[["xen-unstable"]]},{"name":"xsa328/xsa328-4.9-1.patch","project":"xen","versions":[["4.9.x"]]},{"versions":[["4.9.x"]],"project":"xen","name":"xsa328/xsa328-4.9-2.patch"},{"versions":[["4.10.x"],["4.11.x"]],"project":"xen","name":"xsa328/xsa328-4.11-1.patch"},{"name":"xsa328/xsa328-4.11-2.patch","project":"xen","versions":[["4.10.x"],["4.11.x"]]},{"project":"xen","name":"xsa328/xsa328-4.12-1.patch","versions":[["4.12.x"]]},{"versions":[["4.12.x"]],"name":"xsa328/xsa328-4.12-2.patch","project":"xen"},{"versions":[["4.13.x"]],"project":"xen","name":"xsa328/xsa328-4.13-1.patch"},{"versions":[["4.13.x"]],"project":"xen","name":"xsa328/xsa328-4.13-2.patch"}],"public_time":"2020-07-07 12:00","version_time":"2020-07-07 12:23","cve":["CVE-2020-15567"],"title":"non-atomic modification of live EPT PTE","version":3,"xsa":"328"}]}]`,
		[]XSAInfo{{
			XSA:         328,
			Cve:         []string{"CVE-2020-15567"},
			Title:       "non-atomic modification of live EPT PTE",
			PublicTime:  XSATime{Time: time.Date(2020, 7, 7, 12, 0, 0, 0, time.UTC)},
			VersionTime: XSATime{Time: time.Date(2020, 7, 7, 12, 23, 0, 0, time.UTC)},
			Version:     3,
			XenVersionList: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10"),
				xenversion.XenVersion("4.9")},
			ProjectList: []string{"xen"},
			Files: []XSAFileInfo{
				{Name: "xsa328.meta"},
				{Name: "xsa328/xsa328-1.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersionMaster, End: xenversion.XenVersionMaster}}},
				{Name: "xsa328/xsa328-2.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersionMaster, End: xenversion.XenVersionMaster}}},
				{Name: "xsa328/xsa328-4.9-1.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.9"), End: xenversion.XenVersion("4.9")}}},
				{Name: "xsa328/xsa328-4.9-2.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.9"), End: xenversion.XenVersion("4.9")}}},
				{Name: "xsa328/xsa328-4.11-1.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.10"), End: xenversion.XenVersion("4.10")},
						{Start: xenversion.XenVersion("4.11"), End: xenversion.XenVersion("4.11")}}},
				{Name: "xsa328/xsa328-4.11-2.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.10"), End: xenversion.XenVersion("4.10")},
						{Start: xenversion.XenVersion("4.11"), End: xenversion.XenVersion("4.11")}}},
				{Name: "xsa328/xsa328-4.12-1.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.12"), End: xenversion.XenVersion("4.12")}}},
				{Name: "xsa328/xsa328-4.12-2.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.12"), End: xenversion.XenVersion("4.12")}}},
				{Name: "xsa328/xsa328-4.13-1.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.13"), End: xenversion.XenVersion("4.13")}}},
				{Name: "xsa328/xsa328-4.13-2.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.13"), End: xenversion.XenVersion("4.13")}}},
			},
		}},
	},
	{`[{"xsas": [{"xsa":"229","version_time":"2017-08-15 12:04","public_time":"2017-08-15 12:00","cve":["CVE-2017-12134"],"files":[{"name":"xsa229.patch","project":"Linux","versions":[[null]]}],"version":3,"title":"linux: Fix Xen block IO merge-ability calculation"}]}]`,
		[]XSAInfo{{
			XSA:         229,
			Cve:         []string{"CVE-2017-12134"},
			Title:       "linux: Fix Xen block IO merge-ability calculation",
			PublicTime:  XSATime{Time: time.Date(2017, 8, 15, 12, 0, 0, 0, time.UTC)},
			VersionTime: XSATime{Time: time.Date(2017, 8, 15, 12, 4, 0, 0, time.UTC)},
			Version:     3,
			Files: []XSAFileInfo{
				{Name: "xsa229.patch",
					Project:  "Linux",
					Versions: []xenversion.XenVersionRange{{Start: xenversion.XenVersionMin, End: xenversion.XenVersionMaster}}},
			},
			ProjectList: []string{"Linux"},
		}},
	},
	{`[{"xsas": [{"cve":["CVE-2020-15566"],"files":[{"name":"xsa317.meta"},{"name":"xsa317.patch","project":"xen","versions":[["4.10","xen-unstable"]]}],"public_time":"2020-07-07 12:00","title":"Incorrect error handling in event channel port allocation","version":3,"version_time":"2020-07-07 12:18","xsa":"317"}]}]`,
		[]XSAInfo{{
			XSA:         317,
			Cve:         []string{"CVE-2020-15566"},
			Title:       "Incorrect error handling in event channel port allocation",
			PublicTime:  XSATime{Time: time.Date(2020, 7, 7, 12, 0, 0, 0, time.UTC)},
			VersionTime: XSATime{Time: time.Date(2020, 7, 7, 12, 18, 0, 0, time.UTC)},
			Version:     3,
			XenVersionList: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10")},
			ProjectList: []string{"xen"},
			Files: []XSAFileInfo{
				{Name: "xsa317.meta"},
				{Name: "xsa317.patch",
					Project: "xen",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.10"), End: xenversion.XenVersionMaster}}},
			},
		}},
	},
	{`[{"xsas": [{"cve":["CVE-2016-3157"],"files":[{"name":"xsa171.patch","project":"Linux","upstream":"Linux","versions":[["4.4.x"],["4.5-rc7"]]}],"public_time":"2016-03-16 19:00","title":"I/O port access privilege escalation in x86-64 Linux","version":4,"version_time":"2016-03-16 19:03","xsa":"171"},{"cve":["CVE-2021-28039"],"files":[{"name":"xsa369-linux.patch","project":"Linux","upstream":"Linux","versions":[["5.9-stable","5.12-rc"]]}],"public_time":"2021-03-04 10:58","title":"Linux: special config may crash when trying to map foreign pages","version":2,"version_time":"2021-03-05 17:07","xsa":"369"}]}]`,
		[]XSAInfo{{
			XSA:            171,
			Cve:            []string{"CVE-2016-3157"},
			Title:          "I/O port access privilege escalation in x86-64 Linux",
			PublicTime:     XSATime{Time: time.Date(2016, 3, 16, 19, 0, 0, 0, time.UTC)},
			VersionTime:    XSATime{Time: time.Date(2016, 3, 16, 19, 3, 0, 0, time.UTC)},
			Version:        4,
			XenVersionList: []xenversion.XenVersion{},
			ProjectList:    []string{"Linux"},
			Files: []XSAFileInfo{
				{Name: "xsa171.patch",
					Project: "Linux",
					Versions: []xenversion.XenVersionRange{
						{Start: xenversion.XenVersion("4.4"), End: xenversion.XenVersion("4.4")},
						{Start: xenversion.XenVersionMaster, End: xenversion.XenVersionMaster}}},
			}},
			{
				XSA:            369,
				Cve:            []string{"CVE-2021-28039"},
				Title:          "Linux: special config may crash when trying to map foreign pages",
				PublicTime:     XSATime{Time: time.Date(2021, 3, 4, 10, 58, 0, 0, time.UTC)},
				VersionTime:    XSATime{Time: time.Date(2021, 3, 5, 17, 7, 0, 0, time.UTC)},
				Version:        2,
				XenVersionList: []xenversion.XenVersion{},
				ProjectList:    []string{"Linux"},
				Files: []XSAFileInfo{
					{Name: "xsa369-linux.patch",
						Project: "Linux",
						Versions: []xenversion.XenVersionRange{
							{Start: xenversion.XenVersion("5.9"), End: xenversion.XenVersionMaster}}},
				}},
		},
	},
}

func compareXSAInfoList(a, b []XSAInfo, t *testing.T) bool {
	ret := false

	max := len(a)
	if len(a) != len(b) {
		t.Errorf("ERROR: Expected %d XSAs, got %d", len(a), len(b))
		ret = true
		if len(b) < len(a) {
			max = len(b)
		}
	}

	for i := 0; i < max; i++ {
		ret = ret || compareXSAInfo(a[i], b[i], t)
	}

	return ret
}

func compareXSAInfo(a, b XSAInfo, t *testing.T) bool {
	ret := false

	if a.XSA != b.XSA {
		t.Errorf("ERROR: Expecting XSA %d, got %d!", a.XSA, b.XSA)
		ret = true
	}

	if a.Version != b.Version {
		t.Errorf("ERROR: Expecting Version %d, got %d!", a.Version, b.Version)
		ret = true
	}

	if a.Title != b.Title {
		t.Errorf("ERROR: Expecting Title %s, got %s!", a.Title, b.Title)
		ret = true
	}

	if !a.PublicTime.Equal(b.PublicTime.Time) {
		t.Errorf("ERROR: Expecting PublicTime %v, got %v!", a.PublicTime, b.PublicTime)
		ret = true
	}
	if !a.VersionTime.Equal(b.VersionTime.Time) {
		t.Errorf("ERROR: Expecting VersionTime %v, got %v!", a.VersionTime, b.VersionTime)
		ret = true
	}

	if len(a.XenVersionList) != len(b.XenVersionList) {
		t.Errorf("ERROR: XenVersionList: Expecting %v got %v", a.XenVersionList, b.XenVersionList)
		ret = true
	} else {
		for i := range a.XenVersionList {
			if a.XenVersionList[i] != b.XenVersionList[i] {
				t.Errorf("ERROR: XenVersionList: Expecting %v got %v", a.XenVersionList, b.XenVersionList)
				ret = true
				break
			}
		}
	}

	if len(a.ProjectList) != len(b.ProjectList) {
		t.Errorf("ERROR: ProjectList: Expecting %v (len %d) got %v (len %d)",
			a.ProjectList, len(a.ProjectList),
			b.ProjectList, len(b.ProjectList))
		ret = true
	} else {
		for i := range a.ProjectList {
			if a.ProjectList[i] != b.ProjectList[i] {
				t.Errorf("ERROR: ProjectList: Expecting %v got %v", a.ProjectList, b.ProjectList)
				ret = true
				break
			}
		}
	}

	ret = ret || compareXSAFileInfoList(a.Files, b.Files, t)

	return ret
}

func compareXSAFileInfoList(a, b []XSAFileInfo, t *testing.T) bool {
	ret := false

	max := len(a)
	if len(a) != len(b) {
		t.Errorf("ERROR: Expected %d files, got %d", len(a), len(b))
		ret = true
		if len(b) < len(a) {
			max = len(b)
		}
	}

	for i := 0; i < max; i++ {
		ret = ret || compareXSAFileInfo(a[i], b[i], t)
	}

	return ret
}

func compareXSAFileInfo(a, b XSAFileInfo, t *testing.T) bool {
	ret := false

	if a.Name != b.Name {
		t.Errorf("ERROR: Expected Name %s, got %s", a.Name, b.Name)
		ret = true
	}

	return ret
}

func TestUnit(t *testing.T) {
	for i, inst := range inputs {
		xsas, err := GetXSAInfo(Bytes([]byte(inst.b)))
		if err != nil {
			t.Errorf("Unmarshaling [%d]: %v", i, err)
			return
		}
		if compareXSAInfoList(inst.XSAS, xsas, t) {
			t.Errorf("Input %v\nExpected %v\nGot %v",
				inst.b,
				inst.XSAS,
				xsas)
			return
		}
	}
}
