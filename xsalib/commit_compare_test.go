package xsalib

import (
	"testing"

	"github.com/go-git/go-git/v5/plumbing/object"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib/testdata"
)

func TestCommitMessageCompare(t *testing.T) {
	incomplete := map[string]testdata.CommitTestCase{}

	if err := testdata.Init(); err != nil {
		t.Fatalf("Initializing test data: %v", err)
	}

	for _, v := range testdata.CommitTestInfo.Versions {
		t.Logf("Checking commits on branch %v", v)
		xeninfo.CommitsIterate(xeninfo.IterateFullVersionRange(xenversion.XenVersionFullMaster, v.PointRelease(0)),
			func(c *object.Commit) error {
				//t.Logf(" Checking commit %v (%s)", c.Hash, string([]rune(c.Message)[:15]))
				for _, tc := range testdata.CommitTestInfo.TestCases {
					if tc.Version != v {
						continue
					}
					if tc.Hash == "" {
						incomplete[tc.Label] = tc
						continue
					}

					res := compareCommitMessages(tc.CommitMessage, c.Message)
					if tc.Hash == c.Hash.String() {
						t.Logf(" Compared commit %v to alleged patch (%s): result %v", tc.Hash, tc.Label, res)
					}
					switch {
					case tc.Hash == c.Hash.String() && !res:
						t.Errorf("False negative:\n--Patch:\n%v\n--Commit:\n%v",
							tc.CommitMessage, c.Message)
					case tc.Hash != c.Hash.String() && res:
						t.Errorf("False positive:\n--Patch:\n%v\n--Commit:\n%v",
							tc.CommitMessage, c.Message)
					}
				}

				return nil
			})
	}

	for _, tc := range incomplete {
		t.Errorf("Incomplete test data: No hash for %s (%s)", tc.Label, tc.CommitMessage[:30])
	}
}
