package xsalib

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/agnivade/levenshtein"
	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

var commitFilters = []*regexp.Regexp{
	// NB: Sometimes Sob's are removed to protect the identity of discoverers
	regexp.MustCompile("(?m)^(Reported|Acked|Reviewed|Signed-off)-by:.*\n"),
	regexp.MustCompile("(?m)^master (commit|date):.*\n"),
	regexp.MustCompile("(?m)^.*cherry.picked from commit.*\n"),
}

var reDelimit = regexp.MustCompile("(?s)(^.*)\n---\n")

var xsaRegex = regexp.MustCompile(`(m?)^This is.*XSA-[0-9]*\.?$`)

func filterCommit(in string) string {
	out := []byte(in)

	// First, remove a `---` line and anything after it
	if sub := reDelimit.FindSubmatch(out); sub != nil {
		out = sub[1]
	}

	// Then remove "non-functional" tags which are sometimes added on
	// either side
	for _, re := range commitFilters {
		out = re.ReplaceAll(out, []byte{})
	}

	return string(out)
}

func compareCommitMessages(patchMessage, commitMessage string) bool {
	patchMessage = filterCommit(patchMessage)
	commitMessage = filterCommit(commitMessage)

	// Clip the commit message to the same length of the patch
	// message, just to catch any random added tags we may have missed
	// above
	tgtLen := len(commitMessage)
	if len(patchMessage) < tgtLen {
		tgtLen = len(patchMessage)
	}
	commitMessage = commitMessage[:tgtLen]
	patchMessage = patchMessage[:tgtLen]

	patchLines := strings.Split(patchMessage, "\n")
	commitLines := strings.Split(commitMessage, "\n")

	// First lines must be identical
	if patchLines[0] != commitLines[0] {
		return false
	}

	// "This is ... XSA-NNN" lines must be identical
	if xsaRegex.FindString(patchMessage) != xsaRegex.FindString(commitMessage) {
		return false
	}

	d := levenshtein.ComputeDistance(patchMessage, commitMessage)

	// At a basic level, look for things to be less than 50% different
	maxDistance := len(patchMessage) * 50 / 100

	// fmt.Printf("passMessage, filtered:\n%s\ncommitMessage, filtered:\n%s\nlen %d maxdistance %d distance %d\n",
	// 	patchMessage, commitMessage, len(patchMessage), maxDistance, d)

	return d < maxDistance
}

// CompareCommit interprets the file as a patch, and compares the
// patch to a given commit and returns true if they are similar enough
// to be considered the same patch.
//
// At the moment, this is done by computing the Levenshtein distance between the
// commit messages.
func (xfi *XSAFileInfo) CompareCommit(c *object.Commit) (bool, error) {
	// TODO:
	// - The ability to pass your own function to determine similarity
	// - Apply the patch to a tree and see if you get the same tree hash

	_, err := xfi.Message()
	if err != nil {
		return false, err
	}

	return compareCommitMessages(xfi.message, c.Message), nil
}

// Compare a single commit to the file in xfi.  If there's a match, add the commit to
// the given version's list of commit candidates.
func (xfi *XSAFileInfo) commitCandidate(v xenversion.XenVersion, c *object.Commit) error {
	match, err := xfi.CompareCommit(c)

	if err != nil {
		return err
	}

	if match {
		xfi.commits[v] = append(xfi.commits[v], c)
	}

	return nil
}

func commitSearch(v xenversion.XenVersion, files []*XSAFileInfo, fvr xenversion.XenVersionFullRange) error {
	// This looks weird, but it's because iteration goes
	// backwards.  We start with the last version of the range,
	// and end with the first version of the range.  Make sure to pass v,
	// so that we get the Master branch when we want it.
	iteropt := xeninfo.IterateFullVersionRange(fvr.End, fvr.Start, v)

	return xeninfo.CommitsIterate(iteropt,
		func(c *object.Commit) error {
			for i := range files {
				if err := files[i].commitCandidate(v, c); err != nil {
					return fmt.Errorf("commitSearch v %v fvr %v: %v", v, fvr, err)
				}
			}
			return nil
		})
}

// Commits returns "candidate" matches for the given file for a given
// Xen version.  Ideally there should be only one.
func (xfi *XSAFileInfo) Commits(v xenversion.XenVersion) ([]*object.Commit, error) {
	if xfi.Project != "xen" {
		return nil, fmt.Errorf("Don't know how to look up commits for project '%s'", xfi.Project)
	}

	if !xfi.XenVersionList.Present(v) {
		return nil, fmt.Errorf("File not meant for version %v", v)
	}

	if xfi.commits[v] == nil {
		fvr, err := xeninfo.BoundingReleases(v, xfi.xsa.PublicTime.Time)
		if err != nil {
			return nil, fmt.Errorf("Getting bounding releases for version %v at XSA %d public time %v: %v",
				v, xfi.xsa.XSA, xfi.xsa.PublicTime, err)
		}

		if err := commitSearch(v, []*XSAFileInfo{xfi}, fvr); err != nil {
			return nil, err
		}
	}

	return xfi.commits[v], nil
}

// Search for commits on the appropriate version's branch.  Exact
// range will be narrowed by the date of the XSA involved; i.e., if v
// is "4.13", and the XSA was released between 4.13.1 and 4.13.2, only
// those commits will be searched; if the XSA was released after the
// most recent point release, All un-released commits will be checked.
func (xsas XSAList) CommitBatchSearch(v xenversion.XenVersion) error {
	// Need an explicit check or fvr ends up with empty start and end
	// points
	if len(xsas) == 0 {
		return nil
	}

	files := []*XSAFileInfo{}

	fvr := xenversion.XenVersionFullRange{}
	count := 0

	// Normally we expect `xsas` to have already been filtered to
	// include those between one specific set of point releases (e.g.,
	// either between 4.13.2 and 4.13.3, or 4.13.3 and staging-4.13).
	// However, we can't assume that; so find bounding releases for
	// each XSA and merge them together.
	for xi := range xsas {
		xsa := &xsas[xi]

		// We want to be able to batch-search a list of XSAs which contains
		// XSAs before the most recent release.  If the Xen version in
		// question isn't present, just skip it.
		if !xsa.XenVersionList.Present(v) {
			continue
		}

		xsa_fvr, err := xeninfo.BoundingReleases(v, xsa.PublicTime.Time)
		if err != nil {
			return fmt.Errorf("Getting bounding releases for version %v at XSA %d public time %v: %v",
				v, xsa.XSA, xsa.PublicTime, err)
		}

		added := false

		for fi := range xsa.Files {
			file := &xsa.Files[fi]
			if file.Project == "xen" && file.XenVersionList.Present(v) {
				files = append(files, file)
				added = true
			}
		}

		if added {
			err = fvr.Merge(xsa_fvr)
			if err != nil {
				return fmt.Errorf("Merging XenVersionFullRange %v into %v: %v",
					xsa_fvr, fvr, err)
			}
			count++
		}
	}

	if count == 0 {
		// If there are no XSAs for this branch, just skip
		return nil
	}

	if !fvr.Start.Check() || !fvr.End.Check() {
		fmt.Printf("Invalid XenVersionFullRange %v!", fvr)
		panic("Internal error")
	}

	// Call commitSearch
	return commitSearch(v, files, fvr)
}
