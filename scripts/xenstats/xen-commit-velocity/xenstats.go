package main

import (
	"github.com/sirupsen/logrus"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/gitdb"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

func main() {
	gdb, err := gitdb.Open("/Users/georged/tmp/xen-2.gitdb" /*, gitdb.OpenUpdate()*/)
	if err != nil {
		logrus.Fatalf("Opening xen gitdb: %v", err)
	}

	vs, err := xeninfo.VersionList()
	if err != nil {
		logrus.Fatalf("Getting version list: %v", err)
	}

	p, err := plot.New()
	if err != nil {
		logrus.Fatalf("Getting new plot: %v", err)
	}

	//p.Title.Text = "Xen development velocity by additions"
	p.Title.Text = "Xen development velocity by commits"
	p.X.Label.Text = "Days into development window"
	//p.Y.Label.Text = "Lines Added"
	p.Y.Label.Text = "Changes committed"

	plotargs := []interface{}{}

	toVersion := xenversion.XenVersionMin
	for i := range vs {
		var toref, fromref string

		fromVersion := vs[i]

		fromref = fromVersion.PointRelease(0).ReleaseTag()

		if toVersion == xenversion.XenVersionMin {
			toref = "staging"
		} else {
			toref = toVersion.PointRelease(0).ReleaseTag()
		}

		logrus.Infof("Getting points for %v .. %v", fromref, toref)

		//pts, err := gdb.PlotAdditionsCumul(fromref, toref)
		pts, err := gdb.PlotCommitsCumul(fromref, toref)
		if err != nil {
			logrus.Fatalf("Getting points for %v .. %v: %v", fromref, toref, err)
		}

		plotargs = append(plotargs, toref, pts)

		// Version numbers before 4.0.0 aren't reliable
		if fromVersion == xenversion.XenVersion("4.0") {
			break
		}
		toVersion = fromVersion
	}

	err = plotutil.AddLines(p, plotargs...)
	if err != nil {
		logrus.Fatalf("Adding line points to plot: %v", err)
	}

	if err := p.Save(10*vg.Inch, 8*vg.Inch,
		//"/tmp/xen-velocity-additions.svg",
		"/tmp/xen-velocity-commits.svg",
	); err != nil {
		logrus.Fatalf("Saving plot: %v", err)
	}

}
