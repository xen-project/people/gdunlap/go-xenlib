package main

import (
	"encoding/json"
	"html/template"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/gitdb"
	// "gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	// "gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

type SunburstPath struct {
	Name     string          `json:"name"`
	Value    float64         `json:"value,omitempty"`
	Children []*SunburstPath `json:"children,omitempty"`

	splitName []string
	sdMap     map[string]*SunburstPath
}

func sdRecursiveAdd(sd *SunburstPath, elem *SunburstPath) {
	// This is a leaf node; fill in the name and append as-is to the currend subdir
	if len(elem.splitName) == 1 {
		elem.Name = elem.splitName[0]
		sd.Children = append(sd.Children, elem)
		return
	}

	// This is another subdirectory node.
	sdName := elem.splitName[0]

	// Look up the subdir element, creating it if it's not there
	sdElem := sd.sdMap[sdName]
	if sdElem == nil {
		sdElem = &SunburstPath{Name: sdName, sdMap: map[string]*SunburstPath{}}
		// Value 0, no splitName

		sd.Children = append(sd.Children, sdElem)
		sd.sdMap[sdName] = sdElem
	}

	sdRecursiveAdd(sdElem,
		&SunburstPath{splitName: elem.splitName[1:], Value: elem.Value})
}

// Take a top-level SunburstPath with a list of non-refactored
func SunburstRefactorSubtree(stree *SunburstPath) error {
	// Take the list off
	inlist := stree.Children
	stree.Children = nil
	stree.sdMap = map[string]*SunburstPath{}

	for _, elem := range inlist {
		elem.splitName = strings.Split(elem.Name, "/")

		// For the toplevel, always add the original element
		sdRecursiveAdd(stree, elem)
	}

	// FIXME: Go back through and release all the "intermediate" data

	return nil
}

var tmpl = template.Must(template.New("huh").Parse(`
<html>

<head>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-sunburst.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-data-adapter.min.js"></script>
  <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css" type="text/css" rel="stylesheet">
  <link href="https://cdn.anychart.com/releases/v8/fonts/css/anychart-font.min.css" type="text/css" rel="stylesheet">
  <style type="text/css">
    html,
    body,
    #container {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>
</head>

<body>
  <div id="container"></div>
</body>

<script>

anychart.onDocumentReady(function () {
  var data = {{.Data}}

// create a chart and set the data
var chart = anychart.sunburst(data, "as-tree");

// set the calculation mode
chart.calculationMode("parent-independent");

  // set the container id
chart.container("container");

chart.sort("asc");

// initiate drawing the chart
chart.draw();

})

</script>
</html>
`))

func main() {
	fname := "/tmp/starburst.html"
	file, err := os.Create(fname)
	if err != nil {
		logrus.Fatalf("Opening output file: %v", err)
	}
	logrus.Infof("Opened output file %s", fname)

	gdb, err := gitdb.Open("/Users/georged/tmp/xen-2.gitdb" /*, gitdb.OpenUpdate()*/)
	if err != nil {
		logrus.Fatalf("Opening xen gitdb: %v", err)
	}

	stree := &SunburstPath{Name: "xen.git"}

	logrus.Print("Querying files since release 4.17")
	err = gdb.Select(&stree.Children, `
	select filename as name, sum(addition) as value
	    from commitfilestat
	    where commithash in
	        (select commithash
	                from ref_commit_list natural join commits
	                where refname="staging"
	                and commithash not in
	                  (select commithash from ref_commit_list
		           where refname="RELEASE-4.17.0"))
	    group by name
	    order by value desc`)
	// 	err = gdb.Select(&stree.Children, `
	// select filename as name, count(*) as value
	//     from commitfilestat
	//     group by name
	//     order by value desc
	//     limit 50`)

	if err != nil {
		logrus.Fatalf("Getting filename list: %v", err)
	}

	logrus.Print("Refactoring into tree format")

	SunburstRefactorSubtree(stree)

	logrus.Info("Generating json")

	json, err := json.MarshalIndent([]*SunburstPath{stree}, "", " ")
	if err != nil {
		logrus.Fatalf("Marshalling json: %v", err)
	}

	logrus.Info("Generating html")
	err = tmpl.Execute(file, map[string]interface{}{"Data": template.JS(string(json))})
	if err != nil {
		logrus.Fatalf("Generating html output: %v", err)
	}
}
