// xsa-checkin-status-page generates an html page with the XSA
// application status of XSAs.
//
// Usage:
//
//     ./xsa-checkin-status-page > xsa-checkin-status.html
//
// Specifically it:
//
// - Finds all the Xen minor versions which are still security
// supported, and master
//
// - For each of those versions, it finds the most recent point
// release for that branch (most recent .0 release for master)
//
// - Find all XSAs publicly released since that time which have Xen
// patches for that branch
//
// - Searches the git history to see if those patches have been
// applied
//
// - Outputs everything to stdout in HTML form
//
// Both missing and duplicate commits are reported as an error.
// (Duplicate commits are almost certainly due to false positives in
// the patch/commit matching code.)
package main

import (
	"flag"
	"html/template"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

type fileInfo struct {
	Name            string
	FileRowspan     int
	MultipleCommits bool
	Commits         []string
}

type xsaInfo struct {
	XsaRowspan int
	XSA        int
	Title      string
	Files      []fileInfo
	WhyNoFiles string
}

type versionInfo struct {
	XenVersion     xenversion.XenVersion
	XenVersionFull xenversion.XenVersionFull
	Xsas           []xsaInfo
}

var info struct {
	Updated     string
	XenVersions []versionInfo
}

var optOutput = flag.String("o", "-", "Output file; '-' is interpreted as stdout.  Default: stdout")

var statusTemplate = template.Must(template.New("xsaCheckinStatus").Parse(`
<html>
<link
  href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
  rel="stylesheet"
  integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
  crossorigin="anonymous">
<div>
<span class="badge rounded-pill bg-success">Last updated</span> {{.Updated}}
</div>
<div>
<table class="table table-bordered table-hover">
<tr>
{{range .XenVersions}}
<th colspan=4>XSAs for branch {{.XenVersion}} since {{.XenVersionFull}}</th></tr>
<tr>
{{if .Xsas}}
{{range .Xsas}}
<td rowspan={{.XsaRowspan}}>
    <a href=https://xenbits.xenproject.org/xsa/advisory-{{.XSA}}.html>{{.XSA}}</a>
    </td>
<td rowspan={{.XsaRowspan}}>{{.Title}}</td>
  {{if .Files}}
    {{range .Files}}
    <td rowspan={{.FileRowspan}} {{if .MultipleCommits}}class="table-warning"{{end}}>
        <a href=https://xenbits.xenproject.org/xsa/{{.Name}}>{{.Name}}</a></td>
      {{if .Commits}}
        {{range .Commits}}
        <td><a href="https://xenbits.xen.org/gitweb/?p=xen.git;a=commit;h={{.}}">{{.}}</a></td></tr><tr>
        {{end}}
      {{else}}
        <td class="table-danger">NOT FOUND</td></tr><tr>
      {{end}}
    {{end}}
  {{else}}
    <td>{{.WhyNoFiles}}</td></tr><tr>
  {{end}}
{{end}}
{{else}}
<td>No XSAs found</td>
{{end}}
</tr>
{{end}}
</table>
</div>
</html>
`))

func main() {
	var err error
	var output io.Writer

	flag.Parse()

	if *optOutput == "-" {
		log.Printf("Outputting to stdout")
		output = os.Stdout
	} else {
		output, err = os.Create(*optOutput)
		if err != nil {
			log.Fatalf("Opening output file %s: %v", *optOutput, err)
		}
		log.SetOutput(os.Stdout)
		log.Printf("Outputting to %s", *optOutput)
	}

	// Print the time we initiated the update
	info.Updated = time.Now().Format("2006/01/02 15:04:05")

	if err := xeninfo.Update(); err != nil {
		log.Fatalf("Error calling xeninfo.Update(): %v", err)
	}

	// Get all security-supported versions, then add 'master'
	vs, err := xeninfo.VersionList(xeninfo.Window(xeninfo.SecuritySupportWindow))
	if err != nil {
		log.Fatalf("Error getting security-supported version list: %v", err)
	}
	vs = append([]xenversion.XenVersion{xenversion.XenVersionMaster}, vs...)

	info.XenVersions = make([]versionInfo, len(vs))
	for i, v := range vs {
		info.XenVersions[i].XenVersion = v

		info.XenVersions[i].XenVersionFull, err = xeninfo.LatestPointRelease(v)
		if err != nil {
			log.Fatalf("Getting latest point release for version %v: %v", v, err)
		}

		xsas, err := xsalib.GetXSAInfo(xsalib.FilterVersionBranchSince(v))
		if err != nil {
			log.Fatalf("Getting info for uncommitted XSAs on branch %v: %v", v, err)
		}

		if len(xsas) == 0 {
			continue
		}

		log.Printf("Doing batch commit search for branch %v", v)
		err = xsas.CommitBatchSearch(v)
		if err != nil {
			log.Fatalf("Doing batch commit search for version %v: %v", v, err)
		}

		info.XenVersions[i].Xsas = make([]xsaInfo, len(xsas))

		for xi := range xsas {
			xsa := &xsas[xi]
			info.XenVersions[i].Xsas[xi].XSA = xsa.XSA
			info.XenVersions[i].Xsas[xi].Title = xsa.Title
			info.XenVersions[i].Xsas[xi].XsaRowspan = 1 // Default to 1 in case we bail early

			if !xsa.HasProject("xen") {
				info.XenVersions[i].Xsas[xi].WhyNoFiles = "Not a Xen issue"
				continue
			}

			files := xsa.FilterFiles(xsalib.FileFilterVersion(v))

			if len(files) == 0 {
				info.XenVersions[i].Xsas[xi].WhyNoFiles = "No patches for this version"
				continue
			}

			info.XenVersions[i].Xsas[xi].Files = make([]fileInfo, len(files))

			info.XenVersions[i].Xsas[xi].XsaRowspan = 0

			for fi := range files {
				file := &files[fi]
				info.XenVersions[i].Xsas[xi].Files[fi].Name = file.Name

				commits, err := file.Commits(v)
				if err != nil {
					log.Fatalf("Getting commits for file %s, xsa %v, branch %v: %v",
						file.Name, xsa.XSA, v, err)
				}

				if len(commits) == 0 {
					info.XenVersions[i].Xsas[xi].XsaRowspan++
					info.XenVersions[i].Xsas[xi].Files[fi].FileRowspan = 1
					continue
				}

				if len(commits) > 1 {
					info.XenVersions[i].Xsas[xi].Files[fi].MultipleCommits = true
				}

				info.XenVersions[i].Xsas[xi].Files[fi].Commits = make([]string, len(commits))
				for ci := range commits {
					info.XenVersions[i].Xsas[xi].Files[fi].Commits[ci] = commits[ci].Hash.String()

					info.XenVersions[i].Xsas[xi].Files[fi].FileRowspan++
					info.XenVersions[i].Xsas[xi].XsaRowspan++
				}
			}
		}
	}

	if err := statusTemplate.Execute(output, &info); err != nil {
		log.Fatalf("Executing template: %v", err)
	}
}
