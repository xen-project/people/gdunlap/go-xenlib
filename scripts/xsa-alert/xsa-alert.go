// xsa-alert will check for undisclosed XSAs with publication
// times in the future, as well as XSAs published within the last
// three days.  It's designed to be run from a cron job: If there is no
// information, it won't print anything.
package main

import (
	"fmt"
	"os"
	"text/tabwriter"
	"time"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

func FormatXSAList(header string, xsas []xsalib.XSAInfo) {
	if len(xsas) == 0 {
		return
	}

	fmt.Printf("# %s:\n\n", header)

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
	fmt.Fprintf(w, "Date\tXSA\tProject(s)\tDescription\n")
	fmt.Fprintf(w, "----\t---\t----------\t-----------\n")

	for xi := range xsas {
		xsa := &xsas[xi]

		projects := ""
		for i := range xsa.ProjectList {
			if i > 0 {
				projects += ","
			}
			projects += xsa.ProjectList[i]
		}

		fmt.Fprintf(w, "%s\t%d\t%v\t%s\n",
			xsa.PublicTime.Format("Jan 2"),
			xsa.XSA,
			projects,
			xsa.Title)
	}

	w.Flush()

	fmt.Printf("\n")
}

func main() {
	// Filter out "Deallocated" XSAs, as well as any XSAs whose
	// PublicTime is now or earlier.
	xsas, err := xsalib.GetXSAInfo(xsalib.FilterDeallocated(),
		xsalib.FilterBefore(time.Now()))
	if err != nil {
		fmt.Printf("Error getting unreleased XSAs: %v", err)
		os.Exit(1)
	}

	FormatXSAList("UPCOMING XSAs", xsas)

	// Filter out "Deallocated" XSAs, as well as any XSAs whose
	// PublicTime is after now or before one weeks ago.
	xsas, err = xsalib.GetXSAInfo(xsalib.FilterDeallocated(),
		xsalib.FilterBefore(time.Now().AddDate(0, 0, -3)),
		xsalib.FilterAfter(time.Now()))
	if err != nil {
		fmt.Printf("Error getting unreleased XSAs: %v", err)
		os.Exit(1)
	}

	FormatXSAList("Recent XSAs", xsas)

}
