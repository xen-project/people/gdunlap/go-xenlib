# Description

xsa-alert is a script based on the `xenlib` submodules.  It's designed
to be run in a crontab and alert you to recent and upcoming XSA
disclosures.

It's also designed as an example for people to build their own scripts
around the functionality found in the `xenlib` submodules.

It alerts you if there are any upcoming disclosures, or if there has
been a disclosure in the last 3 days.
