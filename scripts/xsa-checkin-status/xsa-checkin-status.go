// xsa-checkin-status checks to make sure that appropriate XSAs have
// been applied to a given version's branch.  Particularly useful to
// be run just before issuing a new release.  Returns 0 on success,
// non-zero on failure.
//
// Usage:
//
//    xsa-checkin-status [version]
//
// Specifically, it:
//
// - Accepts a Xen minor version (defaulting to "master")
//
// - Finds the most recent point release for that branch (most recent .0 release for master)
//
// - Finds all XSAs publicly released since that time which have Xen patches for that branch
//
// - Searches the git history to see of those patches apply.
//
// Both missing and duplicate commits are reported as an error.
// (Duplicate commits are almost certainly due to false positives in
// the patch/commit matching code.)
package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

func xsaCheckinReport(v xenversion.XenVersion) (bool, error) {
	fmt.Printf("Checking for XSAs released since last point release on branch %v\n", v)

	// Get all XSAs for this version
	xsas, err := xsalib.GetXSAInfo(xsalib.FilterVersionBranchSince(v))
	if err != nil {
		return true, fmt.Errorf("Getting info for uncommitted XSAs on branch %v: %w", v, err)
	}

	if len(xsas) == 0 {
		fmt.Printf("No XSAs since last point release on branch %v\n", v)
		return false, nil
	}

	err = xsas.CommitBatchSearch(v)
	if err != nil {
		return true, fmt.Errorf("Doing batch commit search for version %v: %w\n", v, err)
	}

	errorsFound := false

	for xi := range xsas {
		xsa := &xsas[xi]
		fmt.Printf("XSA %d:\n", xsa.XSA)

		// Focus on files for the given version
		files := xsa.FilterFiles(xsalib.FileFilterVersion(v))

		for fi := range files {
			file := &files[fi]
			fmt.Printf("  %s: ", file.Name)
			commits, err := file.Commits(v)
			if err != nil {
				fmt.Printf("Getting commits: %v", err)
				os.Exit(1)
			}

			switch len(commits) {
			case 0:
				fmt.Printf("NOT FOUND\n")
				errorsFound = true
			case 1:
				fmt.Printf("Found (%v)\n", commits[0].Hash)
			default:
				fmt.Printf("ERROR %d candidates\n", len(commits))
				errorsFound = true
			}
		}
	}

	return errorsFound, nil
}

func main() {
	var err error

	// Redirect logs to stdout to be more cron-friendly
	log.SetOutput(os.Stdout)

	v := xenversion.XenVersionMaster

	if len(os.Args) < 2 || os.Args[1] == "-h" {
		fmt.Print(`
Usage:
   xsa-checkin-status <version> [<version>...]

Specifically, it:

- Accepts a series of one or more Xen minor versions (or 'master' for
  the development branch), or version groups.

- Finds the most recent point release for that branch (most recent .0
  release for master)

- Finds all XSAs publicly released since that time which have Xen
  patches for that branch

- Searches the git history to see of those patches apply.

Version groups include:

- 'supported': All currently supported releases

- 'security-supported': All current security-supported releases

Both missing and duplicate commits are reported as an error.
(Duplicate commits are almost certainly due to false positives in
the patch/commit matching code.)

Returns failure either in the event of a processing error, or if it cannot
verify the application status of any XSAs.

Example:

To check that XSAs have been applied to 4.15 in prepartion for a point
release:

    ./xsa-checking-status 4.15

To check that all XSAs have been applied to all appropriate branches:

    ./xsa-checkin-status master security-supported

`)
		os.Exit(1)
	}

	xeninfo.Update()

	errorsFound := false

	for _, arg := range os.Args[1:] {
		vs := []xenversion.XenVersion{}

		switch arg {
		case "security-supported":
			vs, err = xeninfo.VersionList(xeninfo.Window(xeninfo.SecuritySupportWindow))
			if err != nil {
				log.Printf("Getting security-supported versions: %v", err)
				os.Exit(1)
			}
		case "supported":
			vs, err = xeninfo.VersionList(xeninfo.Window(xeninfo.SupportWindow))
			if err != nil {
				log.Printf("Getting supported versions: %v", err)
				os.Exit(1)
			}
		default:
			v, err = xenversion.XenVersionFromString(arg)
			if err != nil {
				log.Printf("Getting xenversion from string: %v", v)
				os.Exit(1)
			}
			vs = []xenversion.XenVersion{v}
		}

		for _, v := range vs {
			errorsFoundNow, err := xsaCheckinReport(v)

			if err != nil {
				log.Printf("Processing error: %v\n", err)
				os.Exit(1)
			}

			errorsFound = errorsFound || errorsFoundNow
		}
	}

	if errorsFound {
		log.Printf("Checkin errors found!\n")
		os.Exit(1)
	}
}
