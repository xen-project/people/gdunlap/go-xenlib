# Description

`xsa-checkin-status` checks to make sure that appropriate XSAs have
been applied to a given version's branch.  Particularly useful to
be run just before issuing a new release.  Returns 0 on success,
non-zero on failure.


Usage:
     xsa-checkin-status [version]

Specifically, it:

  - Accepts a Xen minor version (defaulting to "master")
  - Finds the most recent point release for that branch (most recent .0 release for master)
  - Finds all XSAs publicly released since that time which have Xen patches for that branch
  - Searches the git history to see of those patches apply.

Both missing and duplicate commits are reported as an error.
(Duplicate commits are almost certainly due to false positives in
the patch/commit matching code.)
