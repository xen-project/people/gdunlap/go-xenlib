/*
Copyright © 2020 Citrix Systems UK Ltd.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"

	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

// pointReleaseWebpageCmd represents the pointReleaseWebpage command
var pointReleaseWebpageCmd = &cobra.Command{
	Use:   "pointReleaseWebpage <xenversion>|<xenversionfull>",
	Short: "Generate a webpage for a Xen point release for <xenversion> suitable for posting to xenproject.org",
	Long: `
pointReleaseWebpage takes a Xen release version (either a minor one, such
as 4.13, or a full one, such as 4.13.2) and outputs to stdout HTML designed
to be pasted into a point release download page on xenproject.org.  Logging
messages are printed to stderr; so tool usage for 4.13 would be:

./xen-release pointReleaseWebpage 4.13 > /tmp/release-4.13.html

It will:

* If a minor version is given, find the two most recent point release tags 
(e.g., 4.13.3 and 4.13.2); if a full version is given, find the previous
release (e.g., 4.13.2 -> 4.13.1)

* Download xsa.json, and find all XSAs released between the dates of
  the two release tags (adding 24 hours to the starting release tag, to
  account for security releases)

* Identify all tags which apply to that Xen version (e.g., filtering out
  XSAs which aren't for Xen, or don't have patches for that Xen version)

* Attempt to identify the commits between the two release tags which
  correspond to the patches, giving a report.

The webpage will contain standard links to git repos, a list of all
commits since the last point release, and a table showing the application
status of all XSAs released between the two point releases.

Logged output includes the specific point releases identified, all the XSAs
identified as being released between the two point releases, and either the
commits identified as corresponding with the patches, a warning that commits
cannot be identified, or a reason why the XSA doesn't apply.

Computers being computers, XSA application status may need manual
inspection / correction.
`,
	Args: cobra.ExactArgs(1),
	Run:  pointReleaseWebpage,
}

var optOutput = "-"

func init() {
	rootCmd.AddCommand(pointReleaseWebpageCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// pointReleaseWebpageCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	pointReleaseWebpageCmd.Flags().StringVarP(&optOutput, "output", "o", "-", "Output file; '-' is interpreted as stdout.  Default: stdout")
}

// Put the resulting file in os.TempDir()/xen-release-$v.html

var releaseTemplate = template.Must(template.New("releasePage").Parse(`
<p>We are pleased to announce the release of Xen {{.XenVersionFull}}. This is available immediately from its git repository </p>
<p><a href="https://xenbits.xenproject.org/gitweb/?p=xen.git;a=shortlog;h=refs/heads/{{.StableBranch}}">https://xenbits.xenproject.org/gitweb/?p=xen.git;a=shortlog;h=refs/heads/{{.StableBranch}}</a> (tag <em>{{.ReleaseTag}}</em>) or from this download page</p>
<p>This release contains the following bug-fixes and improvements in the Xen Project hypervisor:</p>
<ul>
{{range .Commits}}
<li><a href="http://xenbits.xen.org/gitweb/?p=xen.git;a=commit;h={{.FullHash}}">{{.ShortHash}}</a>: {{.Oneline}} [{{.Author}}]</li>
{{end}}
</ul>
<p>This release also contains changes to <a href="http://wiki.xenproject.org/wiki/QEMU_Upstream">qemu-upstream</a>, whose changelogs we do not list here as it contains many changes that are not directly releated to the Xen Project Hypervisor and thus this release. However, you can check <a href="https://xenbits.xenproject.org/gitweb/?p=qemu-xen.git;a=shortlog">https://xenbits.xenproject.org/gitweb/?p=qemu-xen.git;a=shortlog</a> (between tags <em>qemu-xen-{{.XenVersionFullPrev}}</em> and <em>qemu-xen-{{.XenVersionFull}}</em>).</p>
<p>This release, which includes source code for qemu-traditional and qemu-upstream, contains the following security fixes. </p>
<table class="zebra">
<tbody>
<tr>
<td width="70px"><strong>XSA</strong></td>
<td width="120px"><strong>Xen</strong></td>
<td width="120px"><strong>qemu-traditional</strong></td>
<td width="120px"><strong>qemu-upstream </strong></td>
</tr>
{{range .XSAs}}
<tr>
<!-- Make bold if one applies, if none applies put why after N/A in () and use ... -->
<td><a href="https://xenbits.xenproject.org/xsa/advisory-{{.XSANum}}.html">{{if .Bold}}<strong>{{end}}XSA-{{.XSANum}}{{if .Bold}}</strong>{{end}}</a></td>
<td>{{if .XenApplied}}<b>Applied</b>{{else}}N/A{{.Excuse}}{{end}}</td>
<td>{{if .QTApplied}}<b>Applied</b>{{else}}N/A{{end}}</td>
<td>{{if .QUApplied}}<b>Applied</b>{{else}}N/A{{end}}</td>
</tr>
{{end}}
</tbody>
</table>
<p>See <a href="https://xenbits.xenproject.org/xsa/">https://xenbits.xenproject.org/xsa/</a> for details related to Xen Project security advisories.</p>
<p>We recommend all users of the {{.XenVersion}} stable series to update to this latest point release.</p>
`))

type CommitInfo struct {
	FullHash, ShortHash, Oneline, Author string
}

type XSAInfo struct {
	Bold                             bool
	XSANum                           int
	XenApplied, QUApplied, QTApplied bool
	Excuse                           string
}

func pointReleaseWebpage(cmd *cobra.Command, args []string) {
	var err error
	var output io.Writer
	if optOutput == "-" {
		output = os.Stdout
	} else {
		output, err = os.Create(optOutput)
		if err != nil {
			log.Fatalf("Opening output file %s: %v", optOutput, err)
		}
		log.SetOutput(os.Stdout)
	}

	var info struct {
		xenversion.XenVersionFull
		XenVersionFullPrev xenversion.XenVersionFull
		xenversion.XenVersion
		StableBranch, ReleaseTag string
		Commits                  []CommitInfo
		XSAs                     []XSAInfo
	}

	// Read & validate Xen version

	// Start by trying to interpret the argument as a full version; if that
	// doesn't work, try to interpret it as a non-full version
	info.XenVersionFull, err = xenversion.XenVersionFullFromString(args[0])
	if err == nil {
		info.XenVersion = info.XenVersionFull.XenVersion()
	} else if info.XenVersion, err = xenversion.XenVersionFromString(args[0]); err != nil {
		log.Fatalf("Getting xenversion from string %s: %v", args[0], err)
	}

	log.Printf("Updating Xen info")

	err = xeninfo.Update()
	if err != nil {
		log.Fatalf("Updating xeninfo: %v", err)
	}

	fvl, err := xeninfo.VersionListFull(xeninfo.FilterXenVersion(info.XenVersion))
	if err != nil {
		log.Fatalf("Getting Xen version list for version %v: %v", info.XenVersion, err)
	}

	if len(fvl) < 2 {
		log.Fatalf("Not enough versions (%v)", fvl)
	}

	// If XenVersionFull isn't valid, then take the most recent point release
	// and its predecessor.  NB we do this here to avoid an extraneous `var` value
	fvStart := fvl[0]
	fvEnd := fvl[1]

	if info.XenVersionFull.Check() {
		// Actually, we have a full version!  Look for that version, and use *its* predecessor
		if info.XenVersionFull.Point() == 0 {
			log.Fatalf("Cannot run on a major release (.0)")
		}

		found := false
		for i := range fvl {
			if fvl[i].XenVersionFull == info.XenVersionFull {
				found = true
				fvStart = fvl[i]
				fvEnd = fvl[i+1]
				break
			}
		}

		if !found {
			log.Fatalf("Could not find full version %v in versionlist (%v)", info.XenVersionFull, fvl)
		}

	}

	info.XenVersionFull = fvStart.XenVersionFull
	info.XenVersionFullPrev = fvEnd.XenVersionFull
	info.StableBranch = info.XenVersion.StableBranch()
	info.ReleaseTag = fvStart.ReleaseTag()

	log.Printf("Looking for commits between %v and %v", fvStart, fvEnd)

	err = xeninfo.CommitsIterate(
		xeninfo.IterateFullVersionRange(fvStart.XenVersionFull,
			fvEnd.XenVersionFull),
		func(c *object.Commit) error {
			mlines := strings.Split(c.Message, "\n")

			info.Commits = append(info.Commits, CommitInfo{
				FullHash:  c.Hash.String(),
				ShortHash: c.Hash.String()[:10],
				Oneline:   mlines[0],
				Author:    c.Author.Name,
			})
			return nil
		})

	log.Printf("Found %v commits", len(info.Commits))

	if err != nil {
		log.Printf("Error processing commits: %v (ignoring)", err)
	}

	// for i := range commits {
	// 	c := commits[i]
	// 	mlines := strings.Split(c.Message, "\n")
	// 	fmt.Printf("%v %v %v\n",
	// 		c.Hash, mlines[0], c.Author.Name)
	// }

	// Take any XSAs with a public release time 24 hours after the tag.  This
	// will catch situations where XSAs were released *as part of* a point
	// release.
	xsas, err := xsalib.GetXSAInfo(xsalib.FilterAfter(fvStart.Time.Add(time.Hour*24)),
		xsalib.FilterBefore(fvEnd.Time))
	if err != nil {
		log.Fatalf("Getting XSAs: %v", err)
	}

	log.Printf("Found %d XSA candidates, doing a batch commit search", len(xsas))

	err = xsas.CommitBatchSearch(info.XenVersion)
	if err != nil {
		log.Fatalf("Doing batch commit search for version %v: %v", info.XenVersion, err)
	}

	missingXSAs := 0

	for i := range xsas {
		xsa := &xsas[i]

		log.Printf("Processing XSA %d", xsa.XSA)

		xi := XSAInfo{XSANum: xsa.XSA}

		if xsa.Deallocated {
			xi.Excuse = " (Unused Number)"

		} else {
			if xsa.HasProject("xen") {
				files := xsa.FilterFiles(xsalib.FileFilterVersion(info.XenVersion))

				if len(files) == 0 {
					log.Printf("(Version not vulnerable)")
					xi.Excuse = " (Version not vulnerable)"
				} else {

					missing := 0

					for fi := range files {
						file := &files[fi]
						commits, err := file.Commits(info.XenVersion)
						if err != nil {
							log.Fatalf("Getting commits for file %s: %v", file.Name, err)
						}

						switch len(commits) {
						case 0:
							log.Printf("  %s: NOT FOUND", file.Name)
							xi.Excuse = " (MISSING)"
							missing++
						case 1:
							log.Printf("  %s: Found (%v)\n", file.Name, commits[0].Hash)
						default:
							hashes := ""
							for i := range commits {
								hashes = fmt.Sprintf("%s%v,", hashes, commits[i].Hash)
							}
							log.Printf("  %s: ERROR multiple candidates (%s)\n", file.Name, hashes)
							missing++
							if xi.Excuse == "" {
								xi.Excuse = " (DUPLICATES, please check)"
							}
						}
					}

					if missing == 0 {
						xi.XenApplied = true
					} else {
						missingXSAs += missing
					}
				}
			} else {
				log.Printf(" (No Xen-related commits)")
			}

			// FIXME: QUApplied and QTApplied once we actually have functioning 'project' values for them

			xi.Bold = xi.XenApplied || xi.QUApplied || xi.QTApplied

			if !xi.Bold && xi.Excuse == "" {
				if len(xsa.ProjectList) == 1 {
					xi.Excuse = fmt.Sprintf(" (%s only)", xsa.ProjectList[0])
				} else {
					xi.Excuse = " (UNKNOWN)"
				}
			}
		}

		info.XSAs = append(info.XSAs, xi)
	}

	if missingXSAs > 0 {
		log.Printf("Missing %d XSA patches!", missingXSAs)
	}

	err = releaseTemplate.Execute(output, &info)
	if err != nil {
		log.Fatalf("Executing template: %v", err)
	}
}
