package gitdb

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/martyros/sqlutil/types/timedb"
)

var XenRepoURL = "xenbits.xen.org/git-http/xen.git"

func TestOpen(t *testing.T) {
	t.Log("Creating temporary file for database")

	dbFile, err := ioutil.TempFile("", "gitdb*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	// TODO: Test opening an empty file w/ no OpenOrigin

	logrus.SetLevel(logrus.InfoLevel)

	gdb, err := Open(dbFile.Name(), OpenOrigin(XenRepoURL), OpenCommitLimitDuration(time.Hour*24*365))
	if err != nil {
		t.Errorf("Opening dbfile: %v", err)
		return
	}

	// Get most recent release ref
	var releaseRef string
	err = gdb.Get(&releaseRef, `
	  select refname from 
	    (select refname, 
			    CAST(substr(refname, 9, 1) as integer) as major, 
				CAST(substr(refname, 11, 2) as integer) as minor 
		  from refs 
		  where reftype="tag" and refname like "RELEASE-%" 
		  order by major desc, minor desc, refname desc limit 1)
	`)
	if err != nil {
		t.Errorf("Getting most recent release: %v", err)
		return
	}

	// Test to make sure filtering works
	gdb.Update()

	{
		t.Log("Running PlotCommitsCumul")
		xys, err := gdb.PlotCommitsCumul(releaseRef, "staging")
		if err != nil {
			t.Errorf("Running PlotCommitsCumul: %v", err)
			return
		}

		for i := range xys {
			if i >= 10 {
				t.Logf(" [more ommitted]")
				break
			}
			t.Logf(" Got plot <%.2f, %.2f>", xys[i].X, xys[i].Y)
		}
	}

	{
		t.Log("Running PlotAdditionsCumul")
		xys, err := gdb.PlotAdditionsCumul(releaseRef, "staging")
		if err != nil {
			t.Errorf("Running PlotAdditionsCumul: %v", err)
			return
		}

		for i := range xys {
			if i >= 10 {
				t.Logf(" [more ommitted]")
				break
			}
			t.Logf(" Got plot <%.2f, %.2f>", xys[i].X, xys[i].Y)
		}
	}

	// Make some test queries
	query := []struct {
		CommitHash              Hash
		CommitterTs             timedb.Time
		Additions, CumAdditions int64
	}{}
	// NB this assumes releases happen more often than once a year.
	err = gdb.Select(&query, `
select commithash, committerts, additions, sum(additions) over rowwindow as cumadditions
   from (select commithash, committerts, sum(addition) as additions
         from (select commithash, committerts
                from ref_commit_list natural join commits
                where refname="staging"
                and commithash not in
                  (select commithash from ref_commit_list
	               where refname=(select refname
                                    from refs natural join commits
                                    where reftype="tag" and refname glob 'RELEASE-*.0'
                                    order by committerts desc
                                    limit 1)))
	     natural join commitfilestat
	     group by commithash)
  window rowwindow as (order by committerts)
  limit 20`)
	if err != nil {
		t.Errorf("Doing cumulative query: %v", err)
		return
	}
	if len(query) == 0 {
		// Make this only a log until we restore the file stats
		//t.Errorf("Expecting at least some rows from query, got %d!", len(query))
		t.Logf("Expecting at least some rows from query, got %d!", len(query))
		return
	}
	for _, q := range query {
		t.Logf("%v %v %v %v", q.CommitHash, q.CommitterTs, q.Additions, q.CumAdditions)
	}

	// TODO: Test opening an existing DB with the wrong OpenOrigin
	gdb.Close()

	t.Logf("Testing re-open (to make sure schema version works correctly)")
	gdb, err = Open(dbFile.Name())
	if err != nil {
		t.Errorf("Re-pening dbfile: %v", err)
		return
	}

	gdb.Close()
}
