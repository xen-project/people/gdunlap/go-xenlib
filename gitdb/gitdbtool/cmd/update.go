/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/gitdb"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update the gitdb using the origin defined when created",
	Run:   Update,
}

func init() {
	rootCmd.AddCommand(updateCmd)

	updateCmd.Flags().StringVarP(&dbfile, "dbfile", "f", "", "Name of database file")
}

func Update(cmd *cobra.Command, args []string) {
	if dbfile == "" {
		logrus.Errorf("Must include both an origin and a dbfile")
	}

	_, err := gitdb.Open(dbfile, gitdb.OpenUpdate())

	if err != nil {
		logrus.Errorf("Updating dbfile %s: %v", dbfile, err)
	}

}
