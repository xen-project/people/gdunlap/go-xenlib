package gitdb

import (
	"database/sql/driver"
	"fmt"

	"github.com/go-git/go-git/v5/plumbing"
)

type Hash struct {
	plumbing.Hash
}

func NewHash(h plumbing.Hash) Hash {
	return Hash{Hash: h}
}

func (h *Hash) Scan(src interface{}) error {
	switch hash := src.(type) {
	case string:
		h.Hash = plumbing.NewHash(hash)
	default:
		return fmt.Errorf("Invalid type type %t", src)
	}
	return nil
}

func (h Hash) Value() (driver.Value, error) {
	return driver.Value(h.String()), nil
}
