package gitdb

import (
	"bufio"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/martyros/sqlutil/liteutil"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/martyros/sqlutil/types/timedb"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/repocache"
)

var errCommitPresent = fmt.Errorf("Commit already present")
var errStopIterate = fmt.Errorf("Stop iteration")

type refSummary struct {
	rtype Reftype
	rname string
	chash plumbing.Hash
}

func (db *GitDB) Update() error {
	origin, err := getAttribute(db, attributeGitOrigin)
	if err != nil {
		return fmt.Errorf("Update failed to get origin: %v", err)
	}

	repo, err := repocache.Open(origin, repocache.OpenUpdate())
	if err != nil {
		return fmt.Errorf("Opening git repo at %s: %v", origin, err)
	}

	// First we get all references from origin
	originRemote, err := repo.Remote("origin")
	if err != nil {
		return fmt.Errorf("Getting remote 'origin': %v", err)
	}

	// On approach would be to use repo.Log(LogOptions{All: true}) to
	// get all commits.  But then we have a problem doing an efficient
	// update: If a committer commits to a local tree and waits for
	// some time before pushing, new commits may appear which are
	// older than commits in the tree.  We could come up with various
	// ways to try to mitigate this, but ultimately they're all
	// heuristics and may fail.
	//
	// Instead, go through each branch and walk backwards until we see
	// a commit that we've seen before.
	refs, err := originRemote.List(&git.ListOptions{})
	if err != nil {
		return fmt.Errorf("Getting remote reference list: %v", err)
	}

	var toRetag []refSummary

	for _, ref := range refs {
		rname := ref.Name()

		logrus.Debugf("Ref name: %v (%v) %v %v", rname, rname.Short(), rname.IsBranch(), rname.IsTag())

		// Filter out anything that's not a branch or tag
		chash := ref.Hash()
		if !rname.IsBranch() {
			continue
		}
		rtype := ReftypeBranch

		logrus.Infof("Looking for commits from ref `%s`", rname)

		iter, err := repo.Log(&git.LogOptions{From: chash, Order: git.LogOrderBSF})
		if err != nil {
			return fmt.Errorf("Getting log for ref %v (%v): %v", rname, chash, err)
		}

		err = iter.ForEach(func(c *object.Commit) error {
			if !db.limitTime.IsZero() && c.Committer.When.Before(db.limitTime) {
				logrus.Infof("Commit %v (ts %v) before limitTime %v, exiting",
					c.Hash, c.Committer.When, db.limitTime)
				return errStopIterate
			}

			logrus.Debugf("Adding commit %v", c)
			err = db.addCommit(c)
			if err != nil {
				if err == errCommitPresent {
					logrus.Debugf("Commit %v present already, ending early", c.Hash)
				} else {
					err = fmt.Errorf("Adding commit: %v", err)
				}
				return err
			}

			return nil
		})

		if err != nil && err != errCommitPresent && err != errStopIterate {
			return fmt.Errorf("Error adding commits: %v", err)
		}

		// FIXME This is probably racy.  For now, only use Update() single-threaded.
		retag, err := db.maybeAddRef(rtype, rname.Short(), chash)
		if err != nil {
			return fmt.Errorf("Error adding ref: %v", err)
		}

		if retag {
			toRetag = append(toRetag, refSummary{rtype, rname.Short(), chash})
		}
	}

	// Now get the tags; no need to add commits, since the tags we
	// care about are all on branches
	refs, err = originRemote.List(&git.ListOptions{})
	if err != nil {
		return fmt.Errorf("Getting remote reference list: %v", err)
	}

TagLoop:
	for _, ref := range refs {
		rname := ref.Name()

		logrus.Debugf("Ref name: %v (%v) %v %v", rname, rname.Short(), rname.IsBranch(), rname.IsTag())

		// Filter out anything that's not a branch or tag
		if !rname.IsTag() {
			continue
		}

		rtype := ReftypeTag

		chash := ref.Hash()
		// Tags are of two types: "lightweight" tags which point
		// directly to commits, and tag objects.  The latter may
		// point to a commit, or may point to another tag object;
		// but Log() only takes a commit hash.  Recursively follow
		// tag objects until you get to a commit object.
		//
		// NB we can't use repo.Resolve() here because it won't
		// resolve tags which point to other tags.
	ObjectLoop:
		for {
			obj, err := repo.Object(plumbing.AnyObject, chash)
			if err != nil {
				if err == plumbing.ErrObjectNotFound {
					logrus.Errorf("Resolving ref %v: object %v not found",
						rname, chash)
					continue TagLoop
				} else {
					return fmt.Errorf("Error getting object %v (tag %s, rname %v): %v",
						ref.Hash(), chash, rname, err)
				}
			}

			switch o := obj.(type) {
			case *object.Commit:
				logrus.Debugf("Found a commit object")
				break ObjectLoop
			case *object.Tag:
				logrus.Debugf("tag: %v -> %v",
					chash, o.Target)
				chash = o.Target
			default:
				return fmt.Errorf("Unexpected object type in tag chain: %T", o)
			}
		}

		retag, err := db.maybeAddRef(rtype, rname.Short(), chash)
		if err != nil {
			return fmt.Errorf("Error adding ref: %v", err)
		}

		if retag {
			toRetag = append(toRetag, refSummary{rtype, rname.Short(), chash})
		}
	}

	logrus.Info("Generating ref commit list")
	err = db.updateRefCommitList(toRetag)
	if err != nil {
		return fmt.Errorf("Error updating ref_commit_list: %v", err)
	}

	return nil
}

// We want to:
// - If the commit doesn't exist, bail w/ no error
// - If the commit exists, upsert the type/rname ref
// - If we either inserted a new value, or changed an old value, re-hash
func (db *GitDB) maybeAddRef(rtype Reftype, rname string, h plumbing.Hash) (bool, error) {
	var retag bool

	err := txutil.TxLoopDb(db.DB, func(eq sqlx.Ext) error {
		var oldHash Hash
		err := sqlx.Get(eq, &oldHash, `
		  select commithash from refs
		    where reftype=? and refname=?
		`, rtype, rname)
		switch {
		case errors.Is(err, sql.ErrNoRows):
			// THe ref doesn't exist; insert it, and mark it as to-be-tagged
			_, err := eq.Exec("insert into refs values(?, ?, ?)", rtype, rname, NewHash(h))
			if liteutil.IsErrorForeignKey(err) {
				// This ref refers to a commit not in the database; do nothing
				return nil
			} else if err != nil {
				return fmt.Errorf("Inserting new ref: %w", err)
			}
			retag = true
			return nil

		case err == nil:
			// The ref exists
			if reflect.DeepEqual(h, oldHash.Hash) {
				// It's unchanged; do nothing (and don't retag)
				return nil
			}
			// It's changed: Update it, and retag
			_, err := eq.Exec("update refs set commithash=? where reftype=? and refname=?",
				NewHash(h), rtype, rname)
			if err != nil {
				return fmt.Errorf("Updating ref: %w", err)
			}
			retag = true
			return nil
		default:
			// Error getting the reference
			return fmt.Errorf("Getting old reftype: %w", err)
		}
	})

	return retag, err
}

type Signature struct {
	PersonalName, MailboxName, HostName string
}

var reEmail = regexp.MustCompile("(.*)@(.*)")

func parseSignature(inSig object.Signature) Signature {
	outSig := Signature{PersonalName: inSig.Name}

	sub := reEmail.FindStringSubmatch(inSig.Email)
	if len(sub) == 0 {
		outSig.MailboxName = "UNKNOWN"
		outSig.HostName = inSig.Email
	} else {
		outSig.MailboxName = sub[1]
		outSig.HostName = sub[2]
	}

	return outSig
}

func getSignatureId(eq sqlx.Ext, sig Signature) (int64, error) {
	var sid int64
	logrus.Debugf("Searching for signature %s <%s@%s>", sig.PersonalName, sig.MailboxName, sig.HostName)
	err := sqlx.Get(eq, &sid, `select sid from signatures where personalname=? and mailboxname=? and hostname=?`,
		sig.PersonalName, sig.MailboxName, sig.HostName)
	if err != nil {
		if err != sql.ErrNoRows {
			err = fmt.Errorf("Getting sid for signature %s <%s@%s>: %w",
				sig.PersonalName, sig.MailboxName, sig.HostName, err)
		} else {
			var res sql.Result
			logrus.Debugf("Inserting signature %s <%s@%s>", sig.PersonalName, sig.MailboxName, sig.HostName)
			res, err = eq.Exec(`insert into signatures(personalname, mailboxname, hostname) values(?, ?, ?)`,
				sig.PersonalName, sig.MailboxName, sig.HostName)
			if err != nil {
				err = fmt.Errorf("Inserting signature %s <%s@%s>: %w",
					sig.PersonalName, sig.MailboxName, sig.HostName, err)
			} else {
				sid, err = res.LastInsertId()
				// If this fails it will be passed up the stack
				logrus.Debugf("After insert: Sid %d err %v", sid, err)
			}
		}
	}

	return sid, err
}

var reSignatureTag = regexp.MustCompile(`^((?:\w+-)*\w+): "?((?:[\pL\p{Pd}.]+ )*[\pL\p{Pd}]+)"? <(.*)@(.*)>`)

func (db *GitDB) addCommit(c *object.Commit) error {
	return db.txLoop(func(eq sqlx.Ext) error {
		var err error

		var commitRow = struct {
			CommitHash   Hash
			AuthorSid    int64
			AuthorTs     timedb.Time
			CommitterSid int64
			CommitterTs  timedb.Time
			PGPSignature string
			Message      string
		}{}

		commitRow.CommitHash = NewHash(c.Hash)

		// Check to see if the commit is already present; if so, let the caller know.
		{
			var count int
			err = sqlx.Get(eq, &count, `select count(*) from commits where commithash=?`,
				commitRow.CommitHash)
			if err != nil {
				return fmt.Errorf("Checking for commit hash presence: %w", err)
			}
			if count == 1 {
				logrus.Infof("Commit hash %v present, skipping", c.Hash)
				return errCommitPresent
			}
		}

		// Look up / insert author
		commitRow.AuthorSid, err = getSignatureId(eq, parseSignature(c.Author))
		if err != nil {
			return err
		}
		commitRow.AuthorTs = timedb.NewTime(c.Author.When)

		// Look up / insert committer
		commitRow.CommitterSid, err = getSignatureId(eq, parseSignature(c.Committer))
		if err != nil {
			return err
		}
		commitRow.CommitterTs = timedb.NewTime(c.Committer.When)

		commitRow.Message = c.Message

		commitRow.PGPSignature = c.PGPSignature

		// Add commit
		_, err = sqlx.NamedExec(eq, `
            insert into commits(commithash,
                    authorsid,
                    authorts,
                    committersid,
                    committerts,
                    message,
                    pgpsignature) 
             values(:commithash,
                    :authorsid,
                    :authorts,
                    :committersid,
                    :committerts,
                    :message,
                    :pgpsignature)`,
			&commitRow)
		if err != nil {
			return err
		}

		// Add parent hash(es)
		for _, h := range c.ParentHashes {
			sh := NewHash(h)
			_, err = eq.Exec(`
				insert into commitjoin(parenthash, childhash)
                values(?, ?)`,
				sh, commitRow.CommitHash)
			if err != nil {
				return nil
			}
		}

		// Look for "signature-based" tags within the commit message
		{
			tagMap := make(map[string][]Signature)
			sc := bufio.NewScanner(strings.NewReader(c.Message))
			for sc.Scan() {
				sub := reSignatureTag.FindStringSubmatch(sc.Text())
				if len(sub) > 0 {
					tagMap[sub[1]] = append(tagMap[sub[1]], Signature{sub[2], sub[3], sub[4]})
				}
			}

			for tag, sigslice := range tagMap {
				for i, sig := range sigslice {
					sid, err := getSignatureId(eq, sig)
					if err != nil {
						return fmt.Errorf("Getting signature for sig %v: %w", sig, err)
					}
					_, err = eq.Exec("insert into commit_signature_tags(tagname, sid, idx, commithash) values(?, ?, ?, ?)",
						tag, sid, i+1, commitRow.CommitHash)
					if err != nil {
						return fmt.Errorf("Inserting tag into commit_signature_tags: %w", err)
					}
				}
			}
		}

		// Add files
		if false {
			emptyFiles := 0

			stats, err := c.Stats()
			if err != nil {
				return err
			}

			for _, stat := range stats {
				// Version 5.2.0 of go-git will set Name to "" in case of a rename; generate
				// an obvious but unique replacement.
				if stat.Name == "" {
					stat.Name = fmt.Sprintf("GITDBEMPTY/%d", emptyFiles)
					logrus.Debugf("Null Name field; using %s", stat.Name)
					emptyFiles++
				}
				errstring := fmt.Sprintf("Inserting <%v, %s, %d, %d> into commitfilestat",
					commitRow.CommitHash,
					stat.Name, stat.Addition, stat.Deletion)
				logrus.Debug(errstring)
				// Add filestat
				_, err = eq.Exec(`
                    insert into commitfilestat(
                        commithash,
                        filename,
                        addition,
                        deletion) values(?, ?, ?, ?)`,
					commitRow.CommitHash,
					stat.Name,
					stat.Addition,
					stat.Deletion)
				if err != nil {
					return fmt.Errorf("%s: %w", errstring, err)
				}
			}
		}

		return nil

	})
}

//lint:ignore U1000 Saving the old way for comparison
func (db *GitDB) updateRefCommitListSql() error {
	return db.txLoop(func(eq sqlx.Ext) error {
		_, err := eq.Exec(`delete from ref_commit_list`)
		if err != nil {
			return fmt.Errorf("Deleting all entries from ref_commit_list: %w", err)
		}

		// Generate transitive closure for references, removing any "dangling"
		// commits caused by partial imports
		_, err = eq.Exec(`
with recursive clist(reftype, refname, commithash)
  as (select reftype, refname, commithash from refs
      union select reftype, refname, parenthash
              from commitjoin, clist
	      where childhash=clist.commithash)
insert into ref_commit_list
  select * from clist
    where commithash not in
      (select parenthash from commitjoin
	      where parenthash not in (select commithash from commits))`)
		if err != nil {
			return fmt.Errorf("Generating entries for ref_commit_list: %w", err)
		}

		return nil
	})
}

func (db *GitDB) updateRefCommitList(refs []refSummary) error {
	if len(refs) == 0 {
		logrus.Info("No updated references, no need to regenerate ref commit list")
		return nil
	}
	return db.txLoop(func(eq sqlx.Ext) error {
		_, err := eq.Exec(`delete from ref_commit_list`)
		if err != nil {
			return fmt.Errorf("Deleting all entries from ref_commit_list: %w", err)
		}

		var commitHashes []struct {
			ChildHash  Hash
			ParentHash Hash
		}

		// Generate transitive closure for references, removing any "dangling"
		// commits caused by partial imports
		err = sqlx.Select(eq, &commitHashes, "select * from commitjoin")
		if err != nil {
			return fmt.Errorf("Getting commit list: %w", err)
		}

		logrus.Infof("Got %d commitjoin entries", len(commitHashes))

		commitMap := make(map[Hash][]Hash)

		for _, r := range commitHashes {
			commitMap[r.ChildHash] = append(commitMap[r.ChildHash], r.ParentHash)
		}

		total := 0
		for _, ref := range refs {
			logrus.Infof("Doing recursive tagging for ref %v (%v)", ref.rname, ref.chash)
			queue := []Hash{}
			chash := NewHash(ref.chash)
			ltotal := 0
			for {
				_, err := eq.Exec("insert into ref_commit_list values(?, ?, ?)",
					ref.rtype, ref.rname, chash)
				if err == nil {
					// If we inserted it successfully, add its parent(s) to be inserted
					queue = append(queue, commitMap[chash]...)
				} else if !liteutil.IsErrorConstraintUnique(err) && !liteutil.IsErrorForeignKey(err) {
					return fmt.Errorf("Inserting ref into ref_commit_list: %w", err)
				}
				// In the case of a UNIQUE constraint violation, we're at the divergence point of a
				// merge whose other branch has already been tagged.
				//
				// In the case of a foreign key violation, we've reached a point where we've gotten back to
				// commits which haven't been imported.
				//
				// In either case, ignore the error, don't attempt to process the parent; but continue with the other
				// unprocessed commits in the queue.

				ltotal++
				total++

				if len(queue) > 0 {
					chash = queue[0]
					queue = queue[1:]
				} else {
					logrus.Infof("Inserted %d references for ref %s (%d total)", ltotal, ref.rname, total)
					break
				}
			}
		}

		return nil
	})
}
