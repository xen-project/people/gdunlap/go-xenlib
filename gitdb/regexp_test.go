package gitdb

import "testing"

func TestReSignatureTag(t *testing.T) {
	tests := []struct {
		input        string
		valid        bool
		tag          string
		personalname string
		username     string
		hostname     string
	}{
		{"Signed-off-by: George Dunlap <george.dunlap@citrix.com>",
			true,
			"Signed-off-by",
			"George Dunlap",
			"george.dunlap",
			"citrix.com"},
		{`Signed-off-by: "George Dunlap" <george.dunlap@citrix.com>`,
			true,
			"Signed-off-by",
			"George Dunlap",
			"george.dunlap",
			"citrix.com"},
		{"Tested-by: Andrew Cooper <andrew.cooper3@citrix.com>",
			true,
			"Tested-by",
			"Andrew Cooper",
			"andrew.cooper3",
			"citrix.com"},
		{"Tested-by: Elliott Mitchell <ehem+xen@m5p.com>",
			true,
			"Tested-by",
			"Elliott Mitchell",
			"ehem+xen",
			"m5p.com"},
		{"Tested-by: Frédéric Pierret <frederic.pierret@qubes-os.org>",
			true,
			"Tested-by",
			"Frédéric Pierret",
			"frederic.pierret",
			"qubes-os.org"},
		{"Tested-by: Marek Marczykowski-Górecki <marmarek@invisiblethingslab.com>",
			true,
			"Tested-by",
			"Marek Marczykowski-Górecki",
			"marmarek",
			"invisiblethingslab.com"},
		{"Suggested-by: Julien Grall <julien@xen.org>",
			true,
			"Suggested-by",
			"Julien Grall",
			"julien",
			"xen.org"},
		{"Suggested-by: Jürgen Groß <jgross@suse.com>",
			true,
			"Suggested-by",
			"Jürgen Groß",
			"jgross",
			"suse.com"},
		{"Suggested-by: Roger Pau Monné <roger.pau@citrix.com>",
			true,
			"Suggested-by",
			"Roger Pau Monné",
			"roger.pau",
			"citrix.com"},
		{"Suggested-by: Samuel Thibault <samuel.thibault@ens-lyon.org>",
			true,
			"Suggested-by",
			"Samuel Thibault",
			"samuel.thibault",
			"ens-lyon.org"},
		{"Signed-off-by: Norbert Kamiński <norbert.kaminski@3mdeb.com>",
			true,
			"Signed-off-by",
			"Norbert Kamiński",
			"norbert.kaminski",
			"3mdeb.com"},
		{"Signed-off-by: Edwin Török <edvin.torok@citrix.com>",
			true,
			"Signed-off-by",
			"Edwin Török",
			"edvin.torok",
			"citrix.com"},
		{"Signed-off-by: Daniel P. Smith <dpsmith@apertussolutions.com>",
			true,
			"Signed-off-by",
			"Daniel P. Smith",
			"dpsmith",
			"apertussolutions.com"},
		{"Reviewed-by: luca.fancellu@arm.com",
			false, "", "", "", ""},
		{"Reviewed-by: Tim Deegan <tim@xen.org> [shadow]",
			true,
			"Reviewed-by",
			"Tim Deegan",
			"tim",
			"xen.org"},
		{"Reviewed-by: Anthony PERARD <anthony.perard@citrix.com>",
			true,
			"Reviewed-by",
			"Anthony PERARD",
			"anthony.perard",
			"citrix.com"},
		{"Reported-by: hanetzer@startmail.com",
			false, "", "", "", ""},
		{"Not-acked-by: Andrew Cooper <andrew.cooper3@citrix.com>",
			true,
			"Not-acked-by",
			"Andrew Cooper",
			"andrew.cooper3",
			"citrix.com"},
		{"CC: george.dunlap@citrix.com",
			false, "", "", "", ""},
		{"CC: Jürgen Groß <jgross@suse.com>",
			true,
			"CC",
			"Jürgen Groß",
			"jgross",
			"suse.com"},
		{"Based-on-patch-by: Julien Grall <julien@xen.org>, Sun Oct 4 20:33:04 2015 +0100",
			true,
			"Based-on-patch-by",
			"Julien Grall",
			"julien",
			"xen.org"},
		{"Begrudingly acked-by: Andrew Cooper <andrew.cooper3@citrix.com>",
			false, "", "", "", ""},
		{"Cc: <stable@vger.kernel.org>",
			false, "", "", "", ""},
	}

	for _, test := range tests {
		t.Logf("Testing input %s", test.input)
		s := reSignatureTag.FindStringSubmatch(test.input)
		if len(s) == 0 {
			if test.valid {
				t.Errorf("ERROR: Expected %s to parse!", test.input)
			}
			continue
		} else if !test.valid {
			t.Errorf("ERROR: Expected %s not to parse, parsed into %v!", test.input, s)
			continue
		} else if len(s) != 5 {
			t.Errorf("ERROR: Unexpected submatch array size %d (%v)", len(s), s)
			continue
		}
		if s[1] != test.tag {
			t.Errorf("ERROR: Expected tag %s, got %s", test.tag, s[1])
		}
		if s[2] != test.personalname {
			t.Errorf("ERROR: Expected tag %s, got %s", test.personalname, s[2])
		}
		if s[3] != test.username {
			t.Errorf("ERROR: Expected tag %s, got %s", test.username, s[3])
		}
		if s[4] != test.hostname {
			t.Errorf("ERROR: Expected tag %s, got %s", test.hostname, s[4])
		}

	}
}
