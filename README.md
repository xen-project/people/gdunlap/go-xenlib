# Description

This module is a collection of packages and scripts designed to make common
Xen-related tasks easier.  It's currently heavily weighted towards the
security process.

# Packages

* [repocache](repocache): A lot of the functionality requires
information which is best gathered from the XenProject git tree. This
package handles keeping a read-only "cache" of such trees available.

* [xenverson](xenversion) A package for dealing with Xen versions.
Some of this functionality will use **repocache** to clone a copy of
`xen.git`.

* [xeninfo](xeninfo) A package to collect information from `xen.git`.

* [xsalib](xsalib) A package to consume information about XSAs.
Currently this focuses primarily on consuming publicly-available
information in
[xsa.json](https://xenbits.xenproject.org/xsa/xsa.json). This may be
extended in the future to process the `xsaNNN.meta` files found in the
advisories, and (for security team members) consume informatino from
`xsa.git`.

# Scripts

This contains some script-like programs which utilize the
functionality above.  Some are given as examples, some are designed to
be used by the community manager / release manager.

* [xsa-alert](scripts/xsa-alert) An example program designed to be run
in a cron job, tha reports upcoming and recently passed XSA embaroes.

* [xsa-checkin-status](scripts/xsa-checkin-status) A tool designed to
be used during the release to make sure that all appropriate XSAs have
been checked in to the tree.
