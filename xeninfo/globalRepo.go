package xeninfo

import (
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/repocache"
)

type xiRepo struct {
	repo       *repocache.CachedRepository
	xenrepourl string
}

var globalRepo = xiRepo{xenrepourl: XenRepoURL}

func (xr *xiRepo) getRepo() (*repocache.CachedRepository, error) {
	if xr.repo == nil {
		var err error
		xr.repo, err = repocache.Open(xr.xenrepourl)

		if err != nil {
			return nil, err
		}
	}

	return xr.repo, nil
}

func (xr *xiRepo) update() error {
	_, err := xr.getRepo()
	if err != nil {
		return err
	}

	return xr.repo.Update()
}
