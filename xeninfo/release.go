// Package to collect information from Xen git repos.  Most operations
// will clone and cache xen.git.
package xeninfo

import (
	"fmt"
	"log"
	"regexp"
	"sort"
	"time"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

type VersionInfo struct {
	Time, SupportUntil, SecuritySupportUntil time.Time
	XenVersion                               xenversion.XenVersion
}

var XenRepoURL = "xenbits.xen.org/git-http/xen.git"

const (
	versionSubsetAll = iota
	versionSubsetSecuritySupported
	versionSubsetSupported
)

type versionListOpt struct {
	xenrepourl string
	at         time.Time
	window     time.Duration
	filters    []func(xenversion.XenVersionFull, time.Time) bool
}

func versionListOptions(options []VersionListOpt) (*versionListOpt, error) {
	opt := &versionListOpt{
		at: time.Now(),
	}
	for _, option := range options {
		err := option(opt)
		if err != nil {
			return opt, fmt.Errorf("Parsing options: %v", err)
		}
	}
	return opt, nil
}

// These functions are used to pass options to VersionInfoList,
// VersionList, and LatestPointRelease
type VersionListOpt func(*versionListOpt) error

// Ask the question as though at time ts: ignore versions after ts,
// and any window or other calculations are done in reference to ts.
func At(ts time.Time) VersionListOpt {
	return func(opt *versionListOpt) error {
		opt.at = ts
		return nil
	}
}

// Filter releases according to a window.  SecuritySupportWindow or
// SupportWindow are particularly useful values here.
func Window(d time.Duration) VersionListOpt {
	return func(opt *versionListOpt) error {
		opt.window = d
		return nil
	}
}

// Filter out all XenVersions not matching the specified one.
// XenVersionMaster will filter out non-x.y.0 releases.
func FilterXenVersion(v xenversion.XenVersion) VersionListOpt {
	return func(opt *versionListOpt) error {
		if v == xenversion.XenVersionMaster {
			opt.filters = append(opt.filters, func(fv xenversion.XenVersionFull, ts time.Time) bool {
				return fv.Point() != 0
			})
		} else {
			opt.filters = append(opt.filters, func(fv xenversion.XenVersionFull, ts time.Time) bool {
				return fv.XenVersion() != v
			})
		}
		return nil
	}
}

var reRelease = regexp.MustCompile("^RELEASE-([0-9]+.[0-9]+.[0-9]+)")

// Security support window is 3 years (36 months)
var SecuritySupportWindow = time.Hour * 24 * 365 * 3

// Normal support window is 18 months
var SupportWindow = time.Hour * 12 * 365 * 3

// Update will clone and/or update the cached repository to the most
// recent version.
func Update() error {
	return globalRepo.update()
}

var stopIterateErr = fmt.Errorf("Loop Break")

func releaseIterate(opt *versionListOpt, f func(fv xenversion.XenVersionFull, ts time.Time) error) error {
	repo, err := globalRepo.getRepo()
	if err != nil {
		return fmt.Errorf("Opening git repo: %v", err)
	}

	iter, err := repo.Tags()
	if err != nil {
		return fmt.Errorf("Getting tags: %v", err)
	}

	// TODO: Check to make sure that there are no duplicate versions
	// or missing versions; these would be indicative of a bug in the
	// regex.

	err = iter.ForEach(func(ref *plumbing.Reference) error {
		sname := ref.Name().Short()
		sub := reRelease.FindSubmatch([]byte(sname))
		if sub == nil {
			return nil
		}

		fv, err := xenversion.XenVersionFullFromString(string(sub[1]))
		if err != nil {
			log.Printf("Parsing version from %s: %v", string(sub[1]), err)
		}

		obj, err := repo.Object(plumbing.AnyObject, ref.Hash())
		if err != nil {
			return fmt.Errorf("Error getting object %v (tag %s): %v\n",
				ref.Hash(), ref.Name(), err)
		}

		var ts time.Time
		switch o := obj.(type) {
		case *object.Commit:
			ts = o.Committer.When
		case *object.Tag:
			ts = o.Tagger.When
		}

		// Skip versions newer than the specified time
		if opt.at.Before(ts) {
			return nil
		}

		if opt.window != 0 {
			// Only return versions currently in the given window
			if opt.at.Sub(ts) > opt.window {
				return nil
			}
		}

		// Apply filters, if any
		for _, f := range opt.filters {
			if f(fv, ts) {
				return nil
			}
		}

		return f(fv, ts)
	})
	iter.Close()

	if err == stopIterateErr {
		err = nil
	}

	return err
}

// Return a list of minor releases, with extra information.  Resulting slice
// is sorted in descending order (most recent releases first).
func VersionInfoList(options ...VersionListOpt) ([]VersionInfo, error) {
	opt, err := versionListOptions(options)
	if err != nil {
		return nil, err
	}

	vl := []VersionInfo{}
	err = releaseIterate(opt, func(fv xenversion.XenVersionFull, ts time.Time) error {
		if fv.Point() != 0 {
			return nil
		}

		vl = append(vl,
			VersionInfo{
				XenVersion:           fv.XenVersion(),
				Time:                 ts,
				SupportUntil:         ts.Add(SupportWindow),
				SecuritySupportUntil: ts.Add(SecuritySupportWindow)})

		return nil
	})

	if err != nil {
		return nil, err
	}

	// NB we sort versions in descending order
	sort.Slice(vl, func(i, j int) bool {
		return vl[i].XenVersion.IsGreaterThan(vl[j].XenVersion)
	})

	return vl, nil
}

// Return a list of minor releases. Resulting slice
// is sorted in descending order (most recent releases first).
func VersionList(options ...VersionListOpt) ([]xenversion.XenVersion, error) {
	opt, err := versionListOptions(options)
	if err != nil {
		return nil, err
	}

	vl := []xenversion.XenVersion{}
	err = releaseIterate(opt, func(fv xenversion.XenVersionFull, ts time.Time) error {
		if fv.Point() != 0 {
			return nil
		}

		vl = append(vl, fv.XenVersion())

		return nil
	})

	if err != nil {
		return nil, err
	}

	sort.Slice(vl, func(i, j int) bool {
		return vl[i].IsGreaterThan(vl[j])
	})

	return vl, nil
}

// LatestPointRelease describes the last point release on the branch
// for a given version.  For Master, it returns the most recent .0
// release.
//
// NB this does mean that for say, 4.14 it could return 4.14.1, and
// for master still return 4.14.0.
func LatestPointRelease(v xenversion.XenVersion, options ...VersionListOpt) (xenversion.XenVersionFull, error) {
	lfv := xenversion.XenVersionFullMin

	opt, err := versionListOptions(options)
	if err != nil {
		return lfv, err
	}

	// For "normal" versions, find the same version with the same X.Y but highest Z
	f := func(fv xenversion.XenVersionFull, ts time.Time) error {
		if fv.XenVersion() == v &&
			(lfv == xenversion.XenVersionFullMin ||
				lfv.Point() < fv.Point()) {
			lfv = fv
		}
		return nil
	}
	if v == xenversion.XenVersionMaster {
		// For master, find the highest X.Y and Z of 0
		f = func(fv xenversion.XenVersionFull, ts time.Time) error {
			if fv.Point() == 0 && fv.XenVersion().IsGreaterThan(lfv.XenVersion()) {
				lfv = fv
			}
			return nil
		}
	}

	err = releaseIterate(opt, f)

	return lfv, err
}

// Given a version "branch" and a timestamp, return a range containing
// the two full versions on either side of that timestamp.
//
// In the case that the timestamp is after the most recent point
// release, the "End" range will be XenVersionFullMaster.
//
// If v is XenVersionMaster, x.y.0 releases will be returned (or
// XenVersionFullMaster, if the timestamp is after the most recent
// minor release).
func BoundingReleases(v xenversion.XenVersion, ts time.Time) (xenversion.XenVersionFullRange, error) {
	fvr := xenversion.XenVersionFullRange{
		xenversion.XenVersionFullInvalid,
		xenversion.XenVersionFullInvalid}

	fvl, err := VersionListFull(FilterXenVersion(v))
	if err != nil {
		return fvr, err
	}

	i := 0
	found := false
	for i = range fvl {
		if fvl[i].Time.Before(ts) {
			found = true
			break
		}
	}

	if !found {
		return fvr, fmt.Errorf("ts %v before earliest release %v at %v",
			ts, fvl[len(fvl)-1].XenVersionFull, fvl[len(fvl)-1].Time)
	}

	fvr.Start = fvl[i].XenVersionFull
	if i > 0 {
		fvr.End = fvl[i-1].XenVersionFull
	} else {
		fvr.End = xenversion.XenVersionFullMaster
	}

	return fvr, nil
}

type VersionFullInfo struct {
	Time time.Time
	xenversion.XenVersionFull
}

// Look up more information about a specific release.
func GetVersionFullInfo(fv xenversion.XenVersionFull, options ...VersionListOpt) (VersionFullInfo, error) {
	fvi := VersionFullInfo{XenVersionFull: fv}

	opt, err := versionListOptions(options)
	if err != nil {
		return fvi, err
	}

	err = releaseIterate(opt, func(rfv xenversion.XenVersionFull, ts time.Time) error {
		if rfv == fv {
			fvi.Time = ts
			return stopIterateErr
		}
		return nil
	})
	return fvi, err
}

// Get a list of point releases (perhaps with some criteria).
func VersionListFull(options ...VersionListOpt) ([]VersionFullInfo, error) {
	opt, err := versionListOptions(options)
	if err != nil {
		return nil, err
	}

	fvis := []VersionFullInfo{}

	err = releaseIterate(opt, func(rfv xenversion.XenVersionFull, ts time.Time) error {
		fvis = append(fvis, VersionFullInfo{ts, rfv})

		return nil
	})

	// NB we sort versions in descending order
	sort.Slice(fvis, func(i, j int) bool {
		return fvis[i].XenVersionFull.IsGreaterThan(fvis[j].XenVersionFull)
	})

	return fvis, err
}

// Given a list of all versions, expand the given XenVersionRange into
// a concrete list of versions.
func ExpandVersionRange(vr xenversion.XenVersionRange, ts time.Time,
	allversions []VersionInfo) []xenversion.XenVersion {

	vs := []xenversion.XenVersion{}

	if vr.End == xenversion.XenVersionMaster {
		vs = append(vs, xenversion.XenVersionMaster)
	}

	for vi := range allversions {
		// Current version; the one under consideration
		cv := &allversions[vi]

		// If the start range is unspecified, skip all
		// versions no longer supported as of this release
		if vr.Start == xenversion.XenVersionMin &&
			cv.SecuritySupportUntil.Before(ts) {
			continue
		}

		// If the end range is unspecified, skip all
		// versions whose .0 hadn't been released yet
		if vr.End == xenversion.XenVersionMaster && cv.Time.After(ts) {
			continue
		}

		// Skip versions older than vr.Start
		if vr.Start.IsGreaterThan(cv.XenVersion) {
			continue
		}

		// If we have a match with vr.Start, append and continue;
		// Or if we're less than or equal to the end version.
		if vr.Start == cv.XenVersion || vr.End.IsGreaterEqualThan(cv.XenVersion) {
			vs = append(vs, cv.XenVersion)
		}
	}

	return vs
}
