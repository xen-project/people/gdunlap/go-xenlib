package xeninfo

import (
	"fmt"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/repocache"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

type iterateOpt struct {
	repo      *repocache.CachedRepository
	starthash *plumbing.Hash
	endfunc   func(c *object.Commit) bool
}

type IterateOpt func(*iterateOpt) error

// Iterate over commits from [start, end) on branch v.  (That is,
// start is included but end is not.)  Commits are processed in
// reverse order, so start must be after end.
//
// If v is a minor release, then start and end must be point releases
// of that minor release; start may also be XenVersionFullMaster,
// which is interpreted as being the tip of staging-$v.  If v is
// XenVersionMaster, then start and end must both be minor releases
// (i.e., the point release is 0); start may also be
// XenVersionFullMaster, which is interpreted as beign staging.
//
// If v is omitted, it will be inferred from start and end.
// XenVersionMaster can never be inferred, and so v should be included
// whenever the target branch may be XenVersionMaster.
func IterateFullVersionRange(start, end xenversion.XenVersionFull, vs ...xenversion.XenVersion) IterateOpt {
	return func(opt *iterateOpt) error {
		var sHash *plumbing.Hash
		var err error

		// Must pass 0 or 1 versions
		if len(vs) > 1 {
			return fmt.Errorf("IterateFullVersionRange: Too many Xen versions (%d)", len(vs))
		}

		// end must always be valid
		if !end.Check() {
			return fmt.Errorf("IterateFullVersionRange: Invalid end '%v'", end)
		}

		if !start.Check() {
			return fmt.Errorf("IterateFullVersionRange: Invalid start '%v'", start)
		}

		var v xenversion.XenVersion

		// If v isn't specified, start with end's XenVersion
		if len(vs) == 1 {
			v = vs[0]
			if !v.Check() {
				return fmt.Errorf("IterateFullVersionRange: Invalid version branch '%v'", v)
			}

			if v == xenversion.XenVersionMaster {
				if end.Point() != 0 {
					return fmt.Errorf("IterateFullVersionRange: end must be a minor release on the master branch")
				}
			} else {
				if end.XenVersion() != v {
					return fmt.Errorf("IterateFullVersionXange: Version mismatch: end '%v' v '%v'",
						end, v)
				}
			}
		} else {
			v = end.XenVersion()
		}

		if start != xenversion.XenVersionFullMaster {
			// Make sure version matches
			if v == xenversion.XenVersionMaster {
				if start.Point() != 0 {
					return fmt.Errorf("IterateFullVersionRange: start must be a minor release on the master branch")
				}
			} else {
				if start.XenVersion() != v {
					return fmt.Errorf("IterateFullVersionRange: Version mismatch: start '%v' v '%v'",
						start, end)
				}
			}

			// Make sure they're in the right order
			if end.IsGreaterThan(start) {
				return fmt.Errorf("start must be greater than end")
			}

			// Get a hash for the start tag.
			sTag := plumbing.Revision(start.ReleaseTag())
			sHash, err = opt.repo.ResolveRevision(sTag)
			if err != nil {
				return fmt.Errorf("Getting hash for %s: %v", sTag, err)
			}
		} else {
			vbranch := plumbing.Revision("origin/" + v.StagingBranch())

			// Get the hash for the staging branch
			if sHash, err = opt.repo.ResolveRevision(vbranch); err != nil {
				return fmt.Errorf("Getting hash for %s: %v", vbranch, err)
			}
		}

		eTag := plumbing.Revision(end.ReleaseTag())
		eHash, err := opt.repo.ResolveRevision(eTag)
		if err != nil {
			return fmt.Errorf("Getting hash for %s: %v", eTag, err)
		}

		opt.starthash = sHash

		if v == xenversion.XenVersionMaster {
			// For master, we have to find the merge base between
			// master and RELEASE-x.y.z; otherwise we'll go all the
			// way back to the beginning.  This is only necessary for
			// end; start will always eventually make its way back to
			// master.
			masterCommit, err := opt.repo.CommitObject(*opt.starthash)
			if err != nil {
				return fmt.Errorf("Getting commit object of master hash %v: %v",
					opt.starthash, err)
			}

			releaseCommit, err := opt.repo.CommitObject(*eHash)
			if err != nil {
				return fmt.Errorf("Getting commit object of last major release %v: %v",
					eHash, err)
			}

			mergelist, err := masterCommit.MergeBase(releaseCommit)
			if err != nil {
				return fmt.Errorf("Getting mergebase for hashes %v and %v: %v",
					opt.starthash, eHash, err)
			}

			if len(mergelist) != 1 {
				return fmt.Errorf("Unexpected hashlist lenght: %d", len(mergelist))
			}

			eHash = &mergelist[0].Hash

		}

		opt.endfunc = func(c *object.Commit) bool {
			return c.Hash == *eHash
		}

		return nil
	}
}

// Iterate over unreleased commits on the stable branch for v.
func IterateVersionUnreleased(v xenversion.XenVersion) IterateOpt {
	fv, err := LatestPointRelease(v)
	if err != nil {
		return func(opt *iterateOpt) error {
			return fmt.Errorf("Getting latest point release for version %v: %v", v, err)
		}
	}

	return IterateFullVersionRange(xenversion.XenVersionFullMaster, fv, v)
}

// CommitsIterate iterates over commits as specified by option.
//
// For example, to iterate over unreleased commits on branch v:
//     CommitsIterate(IterateVersionUnreleased(v),
//         func(c *object.commit) error {
//             ...
//         }
func CommitsIterate(option IterateOpt, f func(c *object.Commit) error) error {
	if option == nil {
		return fmt.Errorf("No options, don't know what to iterate")
	}

	opt := iterateOpt{}
	var err error

	// FIXME: Figure out how to usefully add options here
	if opt.repo, err = repocache.Open(XenRepoURL); err != nil {
		return fmt.Errorf("Opening git repo at %s: %v", XenRepoURL, err)
	}

	if err := option(&opt); err != nil {
		return err
	}

	// Start from starthash and process until we reach the last
	// release.  We have to do breadth-first search to make sure we
	// get all the commits.
	iter, err := opt.repo.Log(&git.LogOptions{From: *opt.starthash, Order: git.LogOrderBSF})
	if err != nil {
		return fmt.Errorf("Getting log for branch: %v", err)
	}

	err = iter.ForEach(func(c *object.Commit) error {
		if opt.endfunc(c) {
			return stopIterateErr
		}

		return f(c)
	})
	iter.Close()

	if err == stopIterateErr {
		err = nil
	}

	return err
}
