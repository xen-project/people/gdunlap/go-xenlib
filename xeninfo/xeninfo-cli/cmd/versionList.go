/*
Copyright © 2021 George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/samber/lo"
	"github.com/spf13/cobra"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xeninfo"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

// versionListCmd represents the versionList command
var versionListCmd = &cobra.Command{
	Use:   "version-list",
	Short: "List Xen versions",
	Run:   versionList,
}

var windowString string

func init() {
	rootCmd.AddCommand(versionListCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// versionListCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// versionListCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	versionListCmd.Flags().StringP("window", "w", "", "Limit to Xen versions within the previous time window.  A number is interpreted as a number of days; 'supported' and 'security-supported' will be interpreted as the appropriate window for fully-supported releases and security-supported releases, respectively.")

	versionListCmd.Flags().StringP("preset", "p", "", "Use a named preset of filters.  Current presets: point-release-webpage (minor only)")

	versionListCmd.Flags().StringP("type", "t", "minor", "Type of release to query.  Options: minor, point.  Default: minor")
}

func pointReleasePostFilter(v xenversion.XenVersion) (bool, error) {
	fvs, err := xeninfo.VersionListFull(xeninfo.FilterXenVersion(v))
	if err != nil {
		return true, fmt.Errorf("Getting full version list for version %v: %w", v, err)
	}

	return len(fvs) <= 1, nil
}

func printSlice[T any](vl []T) {
	for i := range vl {
		if i > 0 {
			fmt.Printf(" ")
		}
		fmt.Printf("%v", vl[i])
	}
	fmt.Printf("\n")
}

func versionListMinor(cmd *cobra.Command, args []string, vlopts []xeninfo.VersionListOpt) {
	var postFilters []func(xenversion.XenVersion) (bool, error)
	pString, err := cmd.Flags().GetString("preset")
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR getting preset: %v", err)
	}
	switch pString {
	case "":
		break
	case "point-release-webpage":
		vlopts = append(vlopts, xeninfo.Window(xeninfo.SupportWindow))
		postFilters = append(postFilters, pointReleasePostFilter)
	default:
		fmt.Fprintf(os.Stderr, "ERROR unknown preset: %s", pString)
	}

	vl, err := xeninfo.VersionList(vlopts...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR getting versionlist: %v\n", err)
		os.Exit(1)
	}

	for _, f := range postFilters {
		var vlPost []xenversion.XenVersion
		for _, v := range vl {
			filter, err := f(v)
			if err != nil {
				fmt.Fprintf(os.Stderr, "ERROR Running filter: %v", err)
				os.Exit(1)
			}
			if !filter {
				vlPost = append(vlPost, v)
			}
		}
		vl = vlPost
	}

	printSlice(vl)
}

func versionListPoint(cmd *cobra.Command, args []string, vlopts []xeninfo.VersionListOpt) {
	var postFilters []func(xeninfo.VersionFullInfo) (bool, error)
	pString, err := cmd.Flags().GetString("preset")
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR getting preset: %v", err)
	}
	switch pString {
	case "":
		break
	// case "point-release-webpage":
	// 	vlopts = append(vlopts, xeninfo.Window(xeninfo.SupportWindow))
	// 	postFilters = append(postFilters, pointReleasePostFilter)
	default:
		fmt.Fprintf(os.Stderr, "ERROR unknown preset: %s", pString)
	}

	vli, err := xeninfo.VersionListFull(vlopts...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR getting versionlist: %v\n", err)
		os.Exit(1)
	}

	for _, f := range postFilters {
		var vlPost []xeninfo.VersionFullInfo
		for _, v := range vli {
			filter, err := f(v)
			if err != nil {
				fmt.Fprintf(os.Stderr, "ERROR Running filter: %v", err)
				os.Exit(1)
			}
			if !filter {
				vlPost = append(vlPost, v)
			}
		}
		vli = vlPost
	}

	vl := lo.Map(vli, func(a xeninfo.VersionFullInfo, _ int) xenversion.XenVersionFull { return a.XenVersionFull })

	printSlice(vl)
}

func versionList(cmd *cobra.Command, args []string) {
	var vlopts []xeninfo.VersionListOpt
	wString, err := cmd.Flags().GetString("window")
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR getting flag window: %v\n", err)
		os.Exit(1)
	}

	switch wString {
	case "":
		break
	case "supported":
		vlopts = append(vlopts, xeninfo.Window(xeninfo.SupportWindow))
	case "security-supported":
		vlopts = append(vlopts, xeninfo.Window(xeninfo.SecuritySupportWindow))
	default:
		if days, err := strconv.Atoi(wString); err == nil {
			vlopts = append(vlopts, xeninfo.Window(time.Duration(days)*time.Hour*12))
		} else {
			fmt.Fprintf(os.Stderr, "ERROR unable to parse window %s: %v\n", wString, err)
			os.Exit(1)
		}
	}

	if update, err := cmd.Flags().GetBool("update"); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR Getting flag update: %v", err)
		os.Exit(1)
	} else if update {
		if err := xeninfo.Update(); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR updating xen.git: %v", err)
			os.Exit(1)
		}
	}

	vtype, err := cmd.Flags().GetString("type")
	switch {
	case err != nil:
		fmt.Fprintf(os.Stderr, "ERROR getting type: %v", err)
		os.Exit(1)
	case vtype == "minor":
		versionListMinor(cmd, args, vlopts)
	case vtype == "point":
		versionListPoint(cmd, args, vlopts)
	default:
		fmt.Fprintf(os.Stderr, "ERROR: Unknown version type %s", vtype)
		os.Exit(1)
	}

}
