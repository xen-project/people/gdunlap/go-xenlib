package xeninfo

import (
	"testing"

	"reflect"
	"sort"
	"time"

	"github.com/mpvl/unique"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

func TestExpandVersionRange(t *testing.T) {
	var allversions = []VersionInfo{
		{XenVersion: xenversion.XenVersion("4.14"),
			Time:                 time.Date(2020, 7, 23, 15, 11, 14, 0, time.UTC),
			SupportUntil:         time.Date(2022, 1, 22, 3, 11, 14, 0, time.UTC),
			SecuritySupportUntil: time.Date(2023, 7, 23, 15, 11, 14, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.13"),
			Time:                 time.Date(2019, 12, 17, 14, 24, 8, 0, time.UTC),
			SupportUntil:         time.Date(2021, 6, 17, 2, 24, 8, 0, time.UTC),
			SecuritySupportUntil: time.Date(2022, 12, 16, 14, 24, 8, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.12"),
			Time:                 time.Date(2019, 4, 1, 11, 5, 9, 0, time.UTC),
			SupportUntil:         time.Date(2020, 9, 29, 23, 5, 9, 0, time.UTC),
			SecuritySupportUntil: time.Date(2022, 3, 31, 11, 5, 9, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.11"),
			Time:                 time.Date(2018, 7, 9, 13, 48, 2, 0, time.UTC),
			SupportUntil:         time.Date(2020, 1, 8, 1, 48, 2, 0, time.UTC),
			SecuritySupportUntil: time.Date(2021, 7, 8, 13, 48, 2, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.10"),
			Time:                 time.Date(2017, 12, 13, 11, 39, 14, 0, time.UTC),
			SupportUntil:         time.Date(2019, 6, 13, 23, 39, 14, 0, time.UTC),
			SecuritySupportUntil: time.Date(2020, 12, 12, 11, 39, 14, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.9"),
			Time:                 time.Date(2017, 6, 27, 18, 15, 3, 0, time.UTC),
			SupportUntil:         time.Date(2018, 12, 27, 6, 15, 3, 0, time.UTC),
			SecuritySupportUntil: time.Date(2020, 6, 26, 18, 15, 3, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.8"),
			Time:                 time.Date(2016, 12, 5, 12, 4, 28, 0, time.UTC),
			SupportUntil:         time.Date(2018, 6, 6, 0, 4, 28, 0, time.UTC),
			SecuritySupportUntil: time.Date(2019, 12, 5, 12, 4, 28, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.7"),
			Time:                 time.Date(2016, 6, 20, 11, 1, 14, 0, time.UTC),
			SupportUntil:         time.Date(2017, 12, 19, 23, 1, 14, 0, time.UTC),
			SecuritySupportUntil: time.Date(2019, 6, 20, 11, 1, 14, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.6"),
			Time:                 time.Date(2015, 10, 7, 10, 36, 1, 0, time.UTC),
			SupportUntil:         time.Date(2017, 4, 6, 22, 36, 1, 0, time.UTC),
			SecuritySupportUntil: time.Date(2018, 10, 6, 10, 36, 1, 0, time.UTC)},
		{XenVersion: xenversion.XenVersion("4.5"),
			Time:                 time.Date(2015, 1, 15, 12, 29, 9, 0, time.UTC),
			SupportUntil:         time.Date(2016, 7, 16, 0, 29, 9, 0, time.UTC),
			SecuritySupportUntil: time.Date(2018, 1, 14, 12, 29, 9, 0, time.UTC)},
	}

	var inputs = []struct {
		vr   xenversion.XenVersionRange
		ts   time.Time
		want []xenversion.XenVersion
	}{
		{vr: xenversion.XenVersionRange{xenversion.XenVersionMaster, xenversion.XenVersionMaster},
			ts: time.Date(2020, 7, 25, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.12"), xenversion.XenVersionMaster},
			ts: time.Date(2020, 7, 25, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.14"),
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.12"), xenversion.XenVersionMaster},
			ts: time.Date(2020, 7, 20, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.12"), xenversion.XenVersion("4.12")},
			ts: time.Date(2020, 7, 20, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersion("4.12"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.12"), xenversion.XenVersion("4.14")},
			ts: time.Date(2020, 7, 20, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersion("4.14"),
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersionMin, xenversion.XenVersion("4.13")},
			ts: time.Date(2020, 7, 20, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.8"), xenversion.XenVersion("4.13")},
			ts: time.Date(2020, 7, 20, 15, 11, 14, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10"),
				xenversion.XenVersion("4.9"),
				xenversion.XenVersion("4.8"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersionMin, xenversion.XenVersionMaster},
			ts: time.Date(2019, 12, 0, 0, 0, 0, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10"),
				xenversion.XenVersion("4.9"),
				xenversion.XenVersion("4.8"),
			}},
		{vr: xenversion.XenVersionRange{xenversion.XenVersion("4.10"), xenversion.XenVersionMaster},
			ts: time.Date(2020, 7, 7, 12, 0, 0, 0, time.UTC),
			want: []xenversion.XenVersion{
				xenversion.XenVersionMaster,
				xenversion.XenVersion("4.13"),
				xenversion.XenVersion("4.12"),
				xenversion.XenVersion("4.11"),
				xenversion.XenVersion("4.10"),
			}},
	}

	for _, input := range inputs {
		got := ExpandVersionRange(input.vr, input.ts, allversions)
		sort.Sort(xenversion.XenVersionSlice(got))
		unique.Unique((*xenversion.XenVersionSlice)(&got))

		if !reflect.DeepEqual(input.want, got) {
			t.Errorf("ERROR: wanted %v, got %v!", input.want, got)
		}
	}
}
