package xeninfo

import (
	"testing"
	"time"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xenversion"
)

var testVersionIndex = 3

func TestVersionListFullNormal(t *testing.T) {
	t.Log("Getting versionlist")

	vl, err := VersionList()
	if err != nil {
		t.Errorf("Getting VersionList: %v", err)
		return
	}

	t.Logf("Found %d versions", len(vl))

	if len(vl) <= testVersionIndex {
		t.Errorf("Version list unexpectedly short! (%d <= %d)",
			len(vl), testVersionIndex)
		return
	}

	v := vl[testVersionIndex]

	t.Logf("Using version %v for test; getting point releases", v)

	fvl, err := VersionListFull(FilterXenVersion(v))
	if err != nil {
		t.Errorf("Getting full version list for version %v: %v", v, err)
		return
	}

	if len(fvl) == 0 {
		t.Errorf("No full versions returned!")
	}

	t.Logf("Point releases found: %v", fvl)

	for i := range fvl {
		if fvl[i].XenVersion() != v {
			t.Errorf("ERROR: full version %v doesn't match filter XenVersion %v!",
				fvl[i], v)
		}
	}
}

func TestVersionListFullMaster(t *testing.T) {
	v := xenversion.XenVersionMaster

	t.Logf("Using version %v for test; getting point releases", v)

	fvl, err := VersionListFull(FilterXenVersion(v))
	if err != nil {
		t.Errorf("Getting full version list for version %v: %v", v, err)
		return
	}

	if len(fvl) == 0 {
		t.Errorf("No full versions returned!")
	}

	t.Logf("Point releases found: %v", fvl)

	for i := range fvl {
		if fvl[i].Point() != 0 {
			t.Errorf("ERROR: full version %v is not a .0 release!",
				fvl[i])
		}
	}
}

func testBoundingReleaseVersion(t *testing.T, v xenversion.XenVersion) {
	t.Logf("Using version %v for test", v)

	fvl, err := VersionListFull(FilterXenVersion(v))
	if err != nil {
		t.Errorf("Getting full version list for version %v: %v", v, err)
		return
	}

	t.Logf("Point releases: %v", fvl)

	if len(fvl) < 2 {
		t.Errorf("Not enough point releases for version %v!", v)
		return
	}

	// Test to make sure that something after the most recent release
	// x gets [x, master]
	ts := fvl[0].Time.Add(24 * time.Hour)

	t.Logf("Getting bounding release at %v (24 hours after %v)",
		ts, fvl[0])
	fvr, err := BoundingReleases(v, ts)
	if err != nil {
		t.Errorf("ERROR: BoundingRelease(%v, %v): %v",
			v, ts, err)
		return
	}

	t.Logf("Got fvr %v", fvr)
	if fvr.Start != fvl[0].XenVersionFull {
		t.Errorf("ERROR: Expecting %v, got %v",
			fvl[0].XenVersionFull, fvr.Start)
		return
	}
	if fvr.End != xenversion.XenVersionFullMaster {
		t.Errorf("ERROR: Expecting XenVersionFullMaster, got %v",
			fvr.End)
		return
	}

	// Test in between the last two releases
	delta := fvl[0].Time.Sub(fvl[1].Time)
	ts = fvl[1].Time.Add(delta / 2)

	t.Logf("Getting bounding releases for %v (halfway between %v and %v)",
		ts, fvl[0], fvl[1])
	fvr, err = BoundingReleases(v, ts)
	if err != nil {
		t.Errorf("ERROR: BoundingRelease(%v, %v): %v",
			v, ts, err)
		return
	}
	t.Logf("Got fvr %v", fvr)

	if fvr.Start != fvl[1].XenVersionFull {
		t.Errorf("ERROR: Expecting %v, got %v",
			fvl[1].XenVersionFull, fvr.Start)
		return
	}
	if fvr.End != fvl[0].XenVersionFull {
		t.Errorf("ERROR: Expecting %v, got %v",
			fvl[0].XenVersionFull, fvr.End)
		return
	}

	// Test before the oldest release; should return an error
	ts = fvl[len(fvl)-1].Time.Add(-24 * time.Hour)

	t.Logf("Getting bounding release at %v (24 hours before oldest release %v), should error",
		ts, fvl[len(fvl)-1])
	fvr, err = BoundingReleases(v, ts)
	if err == nil {
		t.Errorf("ERROR: BoundingRelease(%v, %v) expected error, got %v!",
			v, ts, fvr)
		return
	}

}

func TestBoundingRelease(t *testing.T) {
	vl, err := VersionList()
	if err != nil {
		t.Errorf("Getting VersionList: %v", err)
		return
	}

	if len(vl) <= testVersionIndex {
		t.Errorf("Version list unexpectedly short! (%d <= %d)",
			len(vl), testVersionIndex)
		return
	}

	testBoundingReleaseVersion(t, vl[testVersionIndex])

	testBoundingReleaseVersion(t, xenversion.XenVersionMaster)

}
